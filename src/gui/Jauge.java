package gui;

import jeu.Jeu;
import moteurGraphique.Dessinatrice;

public class Jauge extends ObjetGUI
{
	private float pourcent;
	private String nomValeur;

	public Jauge(String id, boolean visible, boolean clickable, float x, float y, float largeur, float hauteur)
	{
		super("jauge_" + id, visible, clickable, x, y, largeur, hauteur);
		nomValeur = "";
		pourcent = 0;
	}

	@Override
	public void update(int dx, int dy, Jeu jeu)
	{
		pourcent = jeu.getValeur(nomValeur);
	}

	@Override
	public void dessiner(Dessinatrice dessinatrice)
	{
		dessinatrice.dessinerGUI(id);
	}

	public float getPourcent()
	{
		return pourcent;
	}

	
	public void setNomValeur(String nom)
	{
		nomValeur = nom;
	}

}
