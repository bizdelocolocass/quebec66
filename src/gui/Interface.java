package gui;

import java.util.Iterator;
import java.util.LinkedList;

import carte.Carte;
import donnees.InfosBoiteDeDialogue;
import gui.fenetres.*;
import jeu.Jeu;
import moteurGraphique.Dessinatrice;

import java.util.ArrayList;

public class Interface
{
	private ArrayList<ObjetGUI> elements;
	private Dessinatrice dessinatrice;
	private Carte carte;
	
	private FenetreDialogue dialogueActif = null;
	
	public Interface()
	{
		dessinatrice = new Dessinatrice();
		carte = new Carte();
		
		
		elements = new ArrayList<ObjetGUI>();
		
		//tests
		carte.ajouterRegion("gaspesie", dessinatrice);
		carte.ajouterRegion("capitaleNationale", dessinatrice);
		carte.ajouterRegion("basstlaurent", dessinatrice);
		carte.ajouterRegion("centreduquebec", dessinatrice);
		carte.ajouterRegion("estrie", dessinatrice);
		carte.ajouterRegion("cotenord", dessinatrice);
		carte.ajouterRegion("chaudiereAppalaches", dessinatrice);
		carte.ajouterRegion("mauricie", dessinatrice);
		carte.ajouterRegion("lanaudiere", dessinatrice);
		carte.ajouterRegion("laurentides", dessinatrice);
		carte.ajouterRegion("saglac", dessinatrice);
		carte.ajouterRegion("nordduquebec", dessinatrice);
		carte.ajouterRegion("outaouais", dessinatrice);
		carte.ajouterRegion("abitibi", dessinatrice);
		carte.ajouterRegion("monteregieEst", dessinatrice);
		carte.ajouterRegion("monteregieOuest", dessinatrice);
		
		Fenetre fenetreRegion = new FenetreRegion("region", false, true, 50, 60, 300, 225, dessinatrice);
		Fenetre fenetreEmplacement = new FenetreEmplacement("emplacement", false, true, 50, 60, 300, 225, dessinatrice);
		GroupeInterfaceMain base = new GroupeInterfaceMain("base", true, false, 0, 0, 0, 0, dessinatrice);
		
		this.ajouterObjetGUI(base, 0);
		this.ajouterObjetGUI(fenetreRegion, 1);
		this.ajouterObjetGUI(fenetreEmplacement, 2);
		
	}
	
	public void update(Jeu jeu, float xSouris, float ySouris)
	{
		Iterator<ObjetGUI> i = elements.iterator();
		while(i.hasNext())
		{
			i.next().update(0, 0, jeu);
		}
		
		carte.update(clic(xSouris, ySouris));
		if(dialogueActif != null)
		{
			dialogueActif.update(0, 0, jeu);
		}
	}

	public void dessiner()
	{
		Iterator<ObjetGUI> i = elements.iterator();
		while(i.hasNext())
		{
			ObjetGUI element = i.next();
			element.dessiner(dessinatrice);
		}
		carte.dessiner(dessinatrice);
		if(dialogueActif != null)
		{
			dialogueActif.dessiner(dessinatrice);
		}
		dessinatrice.dessinerTout(carte);
	}
	
	public void ajouterObjetGUI(ObjetGUI objet, int z)
	{
		elements.add(z, objet);
		dessinatrice.ajouterObjetGUI(objet.getId(), objet.getX(), objet.getY(), objet.getLargeur(), objet.getHauteur());
	}
	
	/**
	public void ajouterObjetGUIALaFin(ObjetGUI objet)
	{
		elements.add(elements.size(), objet);
	}**/
	
	public String clic(float clicX, float clicY)
	{
		if(dialogueActifOuvert())
		{
			return dialogueActif.clic(clicX, clicY);
		}
		else
		{
			String retour = "";
			
			Iterator<ObjetGUI> i = elements.iterator();
			while(i.hasNext() && retour == "")
			{
				retour = i.next().clic(clicX, clicY);
			}
			
			if(retour == "")
			{
				retour = carte.clic(clicX, clicY);
			}
			
			//� changer p-e
			return retour;
		}
	}
	
	public void ouvrirFenetre(String idFenetre)
	{
		boolean fait = false;
		//fermer toutes les autres fen�tres sauf event
		for(ObjetGUI fenetre: elements)
		{
			if(fenetre.getId() == idFenetre)
			fenetre.setVisible(true);
			fait = true;
		}
		if(fait)
		{
			for(ObjetGUI fenetre: elements)
			{
				if(fenetre.getId() != idFenetre && fenetre.getId() != "fenEvent")
				{
					fenetre.setVisible(false);
				}
			}
		}
	}
	
	public void fermerFenetre(String idFenetre)
	{
		for(ObjetGUI fenetre: elements)
		{
			if(fenetre.getId() == idFenetre)
			fenetre.setVisible(false);

		}
	}
	
	public void zoom(float zoom)
	{
		carte.augmenterZoom(zoom);
	}
	
	public void enHaut()
	{
		carte.enHaut();
	}
	
	public void enBas()
	{
		carte.enBas();
	}
	
	public void aGauche()
	{
		carte.aGauche();
	}
	
	public void aDroite()
	{
		carte.aDroite();
	}
	
	public void nouvelleBoiteDedialogue(InfosBoiteDeDialogue infos)
	{
		if(dialogueActif == null)
		{
			dialogueActif = new FenetreDialogue("fenDialogue", true, true, 40, 30, 330, 240, infos, dessinatrice);
			ajouterObjetGUI(dialogueActif, 3);
		}
		else
		{
			dialogueActif.changerInfos(infos, dessinatrice);
			dialogueActif.setVisible(true);
		}
		
	}
	
	public void fermerFenetreDialogue()
	{
		dialogueActif.setVisible(false);
	}
	
	public boolean dialogueActifOuvert()
	{
		if(dialogueActif == null)
		{
			return false;
		}
		else
		{
			return dialogueActif.isVisible();
		}
	}

	public void nettoyage()
	{
		dessinatrice.nettoyage();
	}
}
