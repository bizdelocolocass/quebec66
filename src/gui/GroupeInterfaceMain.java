package gui;

import java.util.Iterator;

import jeu.Jeu;
import moteurGraphique.Dessinatrice;

/**
 * On met les �l�ments de la fen�tre de base (genre la date mettons)
 *
 */
public class GroupeInterfaceMain extends ObjetGUI
{
	/* Mettre valeurs constantes pour placement de texte heure and stuff parce que ocmme 
	 * ca c'est d�gueux */
	
	
	private String idHeure;
	private String idHeure2;
	private String idMinute;
	private String idMinute2;
	
	public GroupeInterfaceMain(String id, boolean visible, boolean clickable, float x, float y, float largeur,
			float hauteur, Dessinatrice dessin)
	{
		super(id, visible, false, 0, 0, 400, 300);

		
		//Ann�e, heure, etc.
		Texte texteAnnee = new Texte("annee", true, false, 370, 300, 30, 15, "", true, 1.3f);
		this.ajouterObjetGUI(texteAnnee, dessin);
		Texte texteMois= new Texte("mois", true, false, 353, 300, 30, 15, "", true, 1.3f);
		this.ajouterObjetGUI(texteMois, dessin);
		Texte texteJour= new Texte("jour", true, false, 340, 300, 30, 15, "", true, 1.3f);
		this.ajouterObjetGUI(texteJour, dessin);
		Texte texteHeure2= new Texte("null", true, false, 356, 290, 30, 15, "0", false, 1f);
		this.ajouterObjetGUI(texteHeure2, dessin);
		idHeure2 = texteHeure2.getId();
		Texte texteHeure= new Texte("heure", true, false, 360, 290, 15, 15, "", false, 1f);
		this.ajouterObjetGUI(texteHeure, dessin);
		idHeure = texteHeure.getId();
		Texte texteDeuxPoints= new Texte("null", true, false, 364, 290, 30, 15, ":", false, 1f);
		this.ajouterObjetGUI(texteDeuxPoints, dessin);
		idHeure2 = texteHeure2.getId();
		Texte texteMinute = new Texte("minute", true, false, 370, 290, 15, 15, "", false, 1f);
		this.ajouterObjetGUI(texteMinute, dessin);
		idMinute = texteMinute.getId();
		Texte texteMinute2 = new Texte("null", true, false, 366, 290, 15, 15, "0", false, 1f);
		this.ajouterObjetGUI(texteMinute2, dessin);
		idMinute2 = texteMinute2.getId();
		
		/*Exemple pour une jauge
		Jauge jaugePopulariteFemMaterialiste = new Jauge("endroits_gaspe_populariteFemMaterialiste", true, true, 206, 135, 13, 45);
		jaugePopulariteFemMaterialiste.setNomValeur("endroits_gaspe_populariteFemMaterialiste");
		this.ajouterObjetGUI(jaugePopulariteFemMaterialiste, dessin);
		*/
	}

	@Override
	public void update(int dx, int dy, Jeu jeu)
	{
		if(jeu.getTexte("heure").length() == 2)
		{
			for(ObjetGUI objet: contenu)
			{
				if(objet.getId().equalsIgnoreCase(idHeure2))
				{
					objet.setVisible(false);
				}
				else if(objet.getId().equalsIgnoreCase(idHeure))
				{
					if(objet.getX() == 360)
					{
						objet.setX(356);
					}			
				}
			}
		}
		else if(jeu.getTexte("heure").length() == 1)
		{
			for(ObjetGUI objet: contenu)
			{
				if(objet.getId().equalsIgnoreCase(idHeure2))
				{
					objet.setVisible(true);
				}
				else if(objet.getId().equalsIgnoreCase(idHeure))
				{
					if(objet.getX() == 356)
					{
						objet.setX(360);
					}			
				}
			}
		}
		
		if(jeu.getTexte("minute").length() == 2)
		{
			for(ObjetGUI objet: contenu)
			{
				if(objet.getId().equalsIgnoreCase(idMinute2))
				{
					objet.setVisible(false);
				}
				else if(objet.getId().equalsIgnoreCase(idMinute))
				{
					if(objet.getX() == 370)
					{
						objet.setX(366);
					}			
				}
			}
		}
		else if(jeu.getTexte("minute").length() == 1)
		{
			for(ObjetGUI objet: contenu)
			{
				if(objet.getId().equalsIgnoreCase(idMinute2))
				{
					objet.setVisible(true);
				}
				else if(objet.getId().equalsIgnoreCase(idMinute))
				{
					if(objet.getX() == 366)
					{
						objet.setX(370);
					}			
				}
			}
		}
		
		Iterator<ObjetGUI> i = contenu.iterator();
		while(i.hasNext())
		{
			i.next().update(dx, dy, jeu);
		}
	}

	@Override
	public void dessiner(Dessinatrice dessinatrice)
	{
		Iterator<ObjetGUI> i = contenu.iterator();
		while(i.hasNext())
		{
			i.next().dessiner(dessinatrice);
		}
	}

}
