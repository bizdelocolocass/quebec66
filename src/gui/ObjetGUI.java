package gui;

import java.util.ArrayList;

import jeu.Jeu;
import moteurGraphique.Dessinatrice;

public abstract class ObjetGUI
{

	protected String id;
	protected boolean visible;
	protected boolean clickable;
	
	protected float x;
	protected float y;
	protected int z;
	protected float hauteur;
	protected float largeur;
	
	protected ArrayList<ObjetGUI> contenu;
	
	public abstract void update(int dx, int dy, Jeu jeu);
	
	
	
	public ObjetGUI(String id, boolean visible, boolean clickable, float x, float y, float largeur,
			float hauteur)
	{
		super();
		this.id = id;
		this.visible = visible;
		this.clickable = clickable;
		this.x = x;
		this.y = y;
		this.z = 0;
		this.hauteur = hauteur;
		this.largeur = largeur;
		
		this.contenu = new ArrayList<ObjetGUI>();
	}
	
	//retourne id de l'objet cliqu�
	public String clic(float clicX, float clicY)
	{
		String idActuel = "";
		int zActuel = 0;
		
		if(visible)
		{
			for(ObjetGUI objetTemp: contenu)
			{			
				String idTemp = objetTemp.clic(clicX, clicY);
				int zTemp = objetTemp.getZ();
				
				if(idTemp != "" && zTemp > zActuel)
				{
					zActuel = zTemp;
					idActuel = idTemp;
				}
			}
			
			if(idActuel == "" && clicX > this.x && clicX < this.x + this.largeur && clicY > this.y && clicY < this.y + this.hauteur)
			{
				if(clickable)
				{
					idActuel = id;
				}
				else
				{
					idActuel = "";
				}
			}
		}	
		
		return idActuel;

	}
	
	public String hover(int sourisX, int sourisY)
	{
			return "";
	}
	
	public void ajouterObjetGUI(ObjetGUI objet, Dessinatrice dessin)
	{
		contenu.add(objet);
		contenu.get(contenu.size()-1).setZ(this.z+1);
		if(objet.getId().startsWith("texte_"))
		{
			dessin.ajouterTexte((Texte) objet);
		}
		else if(objet.getId().startsWith("jauge_"))
		{
			dessin.ajouterJauge((Jauge) objet);
		}
		else
		{
			dessin.ajouterObjetGUI(objet.getId(), objet.getX(), objet.getY(), objet.getLargeur(), objet.getHauteur());
		}
	}	
	
	public void ajouterObjetGUI(ObjetGUI objet, Dessinatrice dessin, String texture)
	{
		contenu.add(objet);
		contenu.get(contenu.size()-1).setZ(this.z+1);
		if(objet.getId().startsWith("texte_"))
		{
			dessin.ajouterTexte((Texte) objet);
		}
		if(objet.getId().startsWith("jauge_"))
		{
			dessin.ajouterJauge((Jauge) objet);
		}
		else
		{
			dessin.ajouterObjetGUI(objet.getId(), objet.getX(), objet.getY(), objet.getLargeur(), objet.getHauteur(), texture);
		}
	}
	
	//getters et setters
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public boolean isVisible()
	{
		return visible;
	}
	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}
	public boolean isClickable()
	{
		return clickable;
	}
	public void setClickable(boolean clickable)
	{
		this.clickable = clickable;
	}
	public float getX()
	{
		return x;
	}
	public void setX(float x)
	{
		this.x = x;
	}
	public float getY()
	{
		return y;
	}
	public void setY(float y)
	{
		this.y = y;
	}
	public int getZ()
	{
		return z;
	}
	public void setZ(int z)
	{
		this.z = z;
	}
	public float getHauteur()
	{
		return hauteur;
	}
	public void setHauteur(float hauteur)
	{
		this.hauteur = hauteur;
	}
	public float getLargeur()
	{
		return largeur;
	}
	public void setLargeur(float largeur)
	{
		this.largeur = largeur;
	}

	public abstract void dessiner(Dessinatrice dessinatrice);
}
