package gui;

import jeu.Jeu;
import moteurGraphique.Dessinatrice;
import moteurGraphique.fontRendering.TextMaster;

public class Texte extends ObjetGUI
{
	private String texte;
	private boolean centre;
	private String nomValeur;
	private float grosseurTexte = 1f;
	private float r = 0f;
	private float g = 0f;
	private float b = 0f;

	@Override
	public void update(int dx, int dy, Jeu jeu)
	{
		if(nomValeur == "null")
		{
			
		}
		else
		{
			this.texte = jeu.getTexte(nomValeur);
		}
	}

	/**
	 * 
	 * @param nomVariable Nom de la valeur qu'on va chercher dans l'update
	 */
	public Texte(String nomValeur, boolean visible, boolean clickable, float x, float y, float largeur, float hauteur,
			String texte, boolean centre)
	{
		super("texte_" + nomValeur, visible, clickable, x, y, largeur, hauteur);
		if(nomValeur == "null")
		{
			id = "texte_fixe" + TextMaster.creerId();
		}
		
		this.texte = texte;
		this.centre = centre;
		this.nomValeur = nomValeur;
	}
	
	public Texte(String nomValeur, boolean visible, boolean clickable, float x, float y, float largeur, float hauteur,
			String texte, boolean centre, float grosseur)
	{
		super("texte_" + nomValeur, visible, clickable, x, y, largeur, hauteur);
		if(nomValeur == "null")
		{
			id = "texte_fixe" + TextMaster.creerId();
		}
		
		this.grosseurTexte = grosseur;
		this.texte = texte;
		this.centre = centre;
		this.nomValeur = nomValeur;
	}
	
	public Texte(String nomValeur, boolean visible, boolean clickable, float x, float y, float largeur, float hauteur,
			String texte, boolean centre, float grosseur, float r, float g, float b)
	{
		super("texte_" + nomValeur, visible, clickable, x, y, largeur, hauteur);
		if(nomValeur == "null")
		{
			id = "texte_fixe" + TextMaster.creerId();
		}
		
		this.grosseurTexte = grosseur;
		this.texte = texte;
		this.centre = centre;
		this.nomValeur = nomValeur;
		this.r = r;
		this.g = g;
		this.b = b;
	}

	@Override
	public void dessiner(Dessinatrice dessinatrice)
	{
		if(visible)
		{
			dessinatrice.dessinerGUI(id);
		}
		
	}

	public String getTexte()
	{
		return texte;
	}

	public boolean isCentre()
	{
		return centre;
	}

	public void setNomValeur(String nom)
	{
		nomValeur = nom;
	}

	public void setTexte(String texte)
	{
		this.texte = texte;
	}

	public float getGrosseurTexte()
	{
		return grosseurTexte;
	}

	public float getR()
	{
		return r;
	}

	public float getG()
	{
		return g;
	}

	public float getB()
	{
		return b;
	}
	
	
}
