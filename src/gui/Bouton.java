package gui;

import donnees.conditions.Condition;
import jeu.Jeu;
import moteurGraphique.Dessinatrice;

public class Bouton extends ObjetGUI
{
	private Condition condition;
	
	public Bouton(String id, boolean visible, boolean clickable, float x, float y, float hauteur, float largeur, Condition condition)
	{
		super(id, visible, clickable, x, y, hauteur, largeur);
		this.condition = condition;
	}

	//state machine pour quand on clique

	
	@Override
	public void dessiner(Dessinatrice dessinatrice)
	{
		if(visible)
		{
			dessinatrice.dessinerGUI(id);
		}
	}

	@Override
	public void update(int dx, int dy, Jeu jeu)
	{
		x += dx;
		y += dy;
		
		if(condition.tester(jeu)) 
		{
			clickable = true;
		}
		else
		{
			clickable = false;
		}
	}

	@Override
	public void ajouterObjetGUI(ObjetGUI objet, Dessinatrice dessin)
	{
		System.out.println("Ajouter un �l�ment dans un bouton?");
	}
}
