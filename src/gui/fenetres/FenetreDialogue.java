package gui.fenetres;

import donnees.InfosBoiteDeDialogue;
import gui.CarreTexture;
import gui.Texte;
import moteurGraphique.Dessinatrice;

//https://www.youtube.com/watch?v=-4lqNTMb3IY
public class FenetreDialogue extends Fenetre
{
	private final float R = 1;
	private final float G = 1;
	private final float B = 1;
	
	private Texte texteTitre;
	private CarreTexture image;
	private Texte texteDialogue;
	private Texte texteChoix1;
	private CarreTexture boutonChoix1;
	private Texte texteChoix2;
	private CarreTexture boutonChoix2;
	private Texte texteChoix3;
	private CarreTexture boutonChoix3; 
	private Texte texteChoix4;
	private CarreTexture boutonChoix4;
	
	public FenetreDialogue(String id, boolean visible, boolean clickable, float x, float y, float hauteur,
			float largeur, InfosBoiteDeDialogue infos, Dessinatrice dessin)
	{
		super(id, visible, clickable, x, y, hauteur, largeur);
		
		texteTitre = new Texte("null", true, false, 130, 270, 160, 15, infos.getTitre(), true, 2f, R, G, B);
		this.ajouterObjetGUI(texteTitre, dessin);
		
		image = new CarreTexture("carreImage", true, false, 55, 140, 105, 80);
		this.ajouterObjetGUI(image, dessin, "gui/" + infos.getNomImage());
		
		texteDialogue = new Texte("null", true, false, 175, 215, 180, 105, infos.getTextePrincipal(), false, 1f, R, G, B);
		this.ajouterObjetGUI(texteDialogue, dessin);
		
		texteChoix1 = new Texte("null", true, false, 70, 90, 60, 37, infos.getTexteChoix1(), true, 1f, R, G, B);
		this.ajouterObjetGUI(texteChoix1, dessin);
		boutonChoix1 = new CarreTexture("carreChoix1Dialogue", true, true, 70, 45, 60, 15);
		this.ajouterObjetGUI(boutonChoix1, dessin, "gui/boutonEmplacement");
		
		texteChoix2 = new Texte("null", true, false, 145, 90, 60, 37, infos.getTexteChoix2(), true, 1f, R, G, B);
		this.ajouterObjetGUI(texteChoix2, dessin);
		boutonChoix2 = new CarreTexture("carreChoix2Dialogue", true, true, 145, 45, 60, 15);
		this.ajouterObjetGUI(boutonChoix2, dessin, "gui/boutonEmplacement");
		
		texteChoix3 = new Texte("null", true, false, 220, 90, 60, 37, infos.getTexteChoix3(), true, 1f, R, G, B);
		this.ajouterObjetGUI(texteChoix3, dessin);
		boutonChoix3 = new CarreTexture("carreChoix3Dialogue", true, true, 220, 45, 60, 15);
		this.ajouterObjetGUI(boutonChoix3, dessin, "gui/boutonEmplacement");
		
		texteChoix4 = new Texte("null", true, false, 290, 90, 60, 37, infos.getTexteChoix4(), true, 1f, R, G, B);
		this.ajouterObjetGUI(texteChoix4, dessin);
		boutonChoix4 = new CarreTexture("carreChoix4Dialogue", true, true, 290, 45, 60, 15);
		this.ajouterObjetGUI(boutonChoix4, dessin, "gui/boutonEmplacement");
	}

	public void changerInfos(InfosBoiteDeDialogue infos, Dessinatrice dessin)
	{
		texteTitre.setTexte(infos.getTitre());
		
		image = new CarreTexture("carreImage", true, false, 55, 140, 105, 80);
		this.ajouterObjetGUI(image, dessin, "gui/" + infos.getNomImage());
		
		texteDialogue.setTexte(infos.getTextePrincipal());
		
		texteChoix1.setTexte(infos.getTexteChoix1());
		boutonChoix1 = new CarreTexture("carreChoix1Dialogue", true, true, 70, 45, 60, 15);
		this.ajouterObjetGUI(boutonChoix1, dessin, "gui/boutonEmplacement");
		
		texteChoix2.setTexte(infos.getTexteChoix2());
		boutonChoix2 = new CarreTexture("carreChoix2Dialogue", true, true, 145, 45, 60, 15);
		this.ajouterObjetGUI(boutonChoix2, dessin, "gui/boutonEmplacement");
		
		texteChoix3.setTexte(infos.getTexteChoix3());
		boutonChoix3 = new CarreTexture("carreChoix3Dialogue", true, true, 220, 45, 60, 15);
		this.ajouterObjetGUI(boutonChoix3, dessin, "gui/boutonEmplacement");
		
		texteChoix4.setTexte(infos.getTexteChoix4());
		boutonChoix4 = new CarreTexture("carreChoix4Dialogue", true, true, 290, 45, 60, 15);
		this.ajouterObjetGUI(boutonChoix4, dessin, "gui/boutonEmplacement");
	}
}
