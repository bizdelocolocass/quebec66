package gui.fenetres;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.util.Iterator;

import gui.ObjetGUI;
import jeu.Jeu;
import moteurGraphique.Dessinatrice;

public class Fenetre extends ObjetGUI
{

	public Fenetre(String id, boolean visible, boolean clickable, float x, float y, float hauteur, float largeur)
	{
		super(id, visible, clickable, x, y, hauteur, largeur);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void dessiner(Dessinatrice dessinatrice)
	{
		if(visible)
		{
			dessinatrice.dessinerGUI(id);
			Iterator<ObjetGUI> i = contenu.iterator();
			while(i.hasNext())
			{
				i.next().dessiner(dessinatrice);
			}
		}
	}

	@Override
	public void update(int dx, int dy, Jeu jeu)
	{
		if(visible)
		{
			Iterator<ObjetGUI> i = contenu.iterator();
			while(i.hasNext())
			{
				i.next().update(dx, dy, jeu);
			}
		}
	}
	
	@Override
	public String clic(float clicX, float clicY)
	{
		//test
		String clicDedans = super.clic(clicX, clicY);
		if(clicDedans != "")
		{
		}
		return clicDedans;
	}
	
	public void fermer()
	{
		clickable = false;
		visible = false;
	}

	
}
