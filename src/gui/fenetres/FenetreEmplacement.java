package gui.fenetres;

import java.util.Iterator;

import gui.CarreTexture;
import gui.Jauge;
import gui.ObjetGUI;
import gui.Texte;
import jeu.Jeu;
import moteurGraphique.Dessinatrice;

public class FenetreEmplacement extends Fenetre
{
private String nomTextePopNbr;

private final float R = 0.333f;
private final float G = 0.925f;
private final float B = 0.988f;
	
	public FenetreEmplacement(String id, boolean visible, boolean clickable, float x, float y, float hauteur, float largeur, Dessinatrice dessin)
	{
		super(id, visible, clickable, x, y, hauteur, largeur);
		
		
		//Bouton fermer
		CarreTexture boutonFermerEmplacement = new CarreTexture("boutonFermerEmplacement", true, true, 335, 270, 15, 15);
		this.ajouterObjetGUI(boutonFermerEmplacement, dessin, "gui/boutonFermerFenetre");
		
		//Textes
		//Pour ajouter un texte pas dynamique, on d�clare "null" comme nom � la cr�ation
		Texte texteNomRegion = new Texte("endroits_emplacement_nom", true, true, 125, 285, 90, 15, "", true, 1f, R, G, B);
		this.ajouterObjetGUI(texteNomRegion, dessin);
		Texte textePop = new Texte("null", true, false, 51, 270, 90, 15, "Population", false, 1f, R, G, B);
		this.ajouterObjetGUI(textePop, dessin);
		Texte textePopNbr = new Texte("null", true, false, 51, 260, 85, 15, "cul", false, 1f, R, G, B);
		this.ajouterObjetGUI(textePopNbr, dessin);
		nomTextePopNbr = textePopNbr.getId();
		Texte texteDemographie = new Texte("null", true, false, 100, 270, 200, 30, "Demographie", false, 0.9f, R, G, B);
		this.ajouterObjetGUI(texteDemographie, dessin);
		Texte textePopulariteIdeologie = new Texte("null", true, false, 100, 195, 200, 30, "Popularite des ideologies", false, 0.9f, R, G, B);
		this.ajouterObjetGUI(textePopulariteIdeologie, dessin);
		Texte textePopulariteOrg = new Texte("null", true, false, 55, 110, 42, 30, "Popularite des organisations", false, 0.9f, R, G, B);
		this.ajouterObjetGUI(textePopulariteOrg, dessin);
		
		//D�mographie
		Jauge jaugeRatioFemmes = new Jauge("endroits_emplacement_ratioFemmes", true, true, 101, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioFemmes , dessin);
		Jauge jaugeRatioFranco = new Jauge("endroits_emplacement_ratioFranco", true, true, 116, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioFranco, dessin);
		Jauge jaugeRatioAnglo = new Jauge("endroits_emplacement_ratioAnglo", true, true, 131, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioAnglo, dessin);
		Jauge jaugeRatioLGBTQIA = new Jauge("endroits_emplacement_ratioLGBTQIA", true, true, 146, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioLGBTQIA, dessin);
		Jauge jaugeRatioProletaires = new Jauge("endroits_emplacement_ratioProletaires", true, true, 161, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioProletaires, dessin);
		Jauge jaugeRatioPaysannes = new Jauge("endroits_emplacement_ratioPaysannes", true, true, 176, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioPaysannes , dessin);
		Jauge jaugeRatioBourgeoises = new Jauge("endroits_emplacement_ratioBourgeoises", true, true, 191, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioBourgeoises, dessin);
		Jauge jaugeRatioPetitesBourgeoises = new Jauge("endroits_emplacement_ratioPetitesBourgeoises", true, true, 206, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioPetitesBourgeoises , dessin);
		Jauge jaugeRatioIntellectuelles = new Jauge("endroits_emplacement_ratioIntellectuelles", true, true, 221, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioIntellectuelles, dessin);
		Jauge jaugeRatioArabes = new Jauge("endroits_emplacement_ratioArabes", true, true, 236, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioArabes, dessin);
		Jauge jaugeRatioLatines = new Jauge("endroits_emplacement_ratioLatines", true, true, 251, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioLatines, dessin);
		Jauge jaugeRatioNoires = new Jauge("endroits_emplacement_ratioNoires", true, true, 266, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioNoires, dessin);
		Jauge jaugeRatioAsiatiques = new Jauge("endroits_emplacement_ratioAsiatiques", true, true, 281, 210, 13, 45);
		this.ajouterObjetGUI(jaugeRatioAsiatiques, dessin);
		
		//Popularit� id�ologie
		Jauge jaugePopulariteMarxisme = new Jauge("endroits_emplacement_populariteMarxisme", true, true, 101, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteMarxisme, dessin);
		CarreTexture carreMarxisme = new CarreTexture("carreMarxisme", true, false, 101.5f, 122, 12,12);
		this.ajouterObjetGUI(carreMarxisme, dessin, "emblemes/marxisme1");
		Jauge jaugePopulariteLeninisme = new Jauge("endroits_emplacement_populariteLeninisme", true, true, 116, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteLeninisme, dessin);
		CarreTexture carreLeninisme = new CarreTexture("carreLeninisme", true, false, 116.5f, 122, 12,12);
		this.ajouterObjetGUI(carreLeninisme, dessin, "emblemes/leninisme2");
		Jauge jaugePopulariteML = new Jauge("endroits_emplacement_populariteML", true, true, 131, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteML, dessin);
		CarreTexture carreML = new CarreTexture("carreML", true, false, 131.5f, 122, 12,12);
		this.ajouterObjetGUI(carreML, dessin, "emblemes/ml1");
		Jauge jaugePopulariteBL = new Jauge("endroits_emplacement_populariteBL", true, true, 146, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteBL, dessin);
		CarreTexture carreBolchevisme = new CarreTexture("carreBolchevisme", true, false, 146.5f, 122, 12,12);
		this.ajouterObjetGUI(carreBolchevisme, dessin, "emblemes/bolchevisme1");
		Jauge jaugePopulariteSocDem = new Jauge("endroits_emplacement_populariteSocDem", true, true, 161, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteSocDem, dessin);
		CarreTexture carreSocDem = new CarreTexture("carreSocDem", true, false, 161.5f, 122, 12,12);
		this.ajouterObjetGUI(carreSocDem, dessin, "emblemes/socDem1");
		Jauge jaugePopulariteMao = new Jauge("endroits_emplacement_populariteMao", true, true, 176, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteMao, dessin);
		CarreTexture carreMaoisme = new CarreTexture("carreMaoisme", true, false, 176.5f, 122, 12,12);
		this.ajouterObjetGUI(carreMaoisme, dessin, "emblemes/mao1");
		Jauge jaugePopulariteFeminisme = new Jauge("endroits_emplacement_populariteFeminisme", true, true, 191, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteFeminisme, dessin);
		CarreTexture carreFeminisme = new CarreTexture("carreFeminisme", true, false, 191.5f, 122, 12,12);
		this.ajouterObjetGUI(carreFeminisme, dessin, "emblemes/feminisme3");
		Jauge jaugePopulariteFemMaterialiste = new Jauge("endroits_yo_populariteFemMaterialiste", true, true, 206, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteFemMaterialiste, dessin);
		CarreTexture carreFeminismeMaterialiste = new CarreTexture("carreFeminismeMaterialiste", true, false, 206.5f, 122, 12,12);
		this.ajouterObjetGUI(carreFeminismeMaterialiste, dessin, "emblemes/lesbianisme1");
		Jauge jaugePopulariteNationalisme = new Jauge("endroits_emplacement_populariteNationalisme", true, true, 221, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteNationalisme, dessin);
		CarreTexture carreNationalisme = new CarreTexture("carreNationalisme", true, false, 221.5f, 122, 12,12);
		this.ajouterObjetGUI(carreNationalisme, dessin, "emblemes/nationalisme1");
		Jauge jaugePopulariteLiberation = new Jauge("endroits_emplacement_populariteLiberation", true, true, 236, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteLiberation, dessin);
		CarreTexture carreLiberation = new CarreTexture("carreLiberation", true, false, 236.5f, 122, 12,12);
		this.ajouterObjetGUI(carreLiberation, dessin, "emblemes/liberation1");
		Jauge jaugePopularitePacifisme = new Jauge("endroits_emplacement_popularitePacifisme", true, true, 251, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopularitePacifisme, dessin);
		CarreTexture carrePacifisme = new CarreTexture("carrePacifisme", true, false, 251.5f, 122, 12,12);
		this.ajouterObjetGUI(carrePacifisme, dessin, "emblemes/pacifisme1");
		Jauge jaugePopulariteAnarchisme = new Jauge("endroits_emplacement_populariteAnarchisme", true, true, 266, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteAnarchisme, dessin);
		CarreTexture carreAnarchisme = new CarreTexture("carreAnarchisme", true, false, 266.5f, 122, 12,12);
		this.ajouterObjetGUI(carreAnarchisme, dessin, "emblemes/anarchisme1");
		Jauge jaugePopulariteBlackPower = new Jauge("endroits_emplacement_populariteBlackPower", true, true, 281, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteBlackPower, dessin);
		Jauge jaugePopulariteRedPower = new Jauge("endroits_emplacement_populariteRedPower", true, true, 296, 135, 13, 45);
		this.ajouterObjetGUI(jaugePopulariteRedPower, dessin);
		
		//Popularit� des organisations
		Jauge jaugeMembresFlq = new Jauge("endroits_emplacement_sympathieFlq", true, true, 101, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresFlq, dessin);
		CarreTexture carreFlq = new CarreTexture("carreFLQ", true, false, 101.5f, 62, 12,12);
		this.ajouterObjetGUI(carreFlq, dessin, "emblemes/patriote2");
		Jauge jaugeMembresPsq = new Jauge("endroits_emplacement_sympathiePsq", true, true, 116, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresPsq, dessin);
		Jauge jaugeMembresLso = new Jauge("endroits_emplacement_sympathieLso", true, true, 131, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresLso, dessin);
		Jauge jaugeMembresPcq = new Jauge("endroits_emplacement_sympathiePcq", true, true, 146, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresPcq, dessin);
		Jauge jaugeMembresFlf = new Jauge("endroits_emplacement_sympathieFlf", true, true, 161, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresFlf, dessin);
		CarreTexture carreFlf = new CarreTexture("carreFlf", true, false, 161.5f, 62, 12,12);
		this.ajouterObjetGUI(carreFlf, dessin, "emblemes/flf1");
		Jauge jaugeMembresIopq = new Jauge("endroits_emplacement_sympathieIopq", true, true, 176, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresIopq, dessin);
		CarreTexture carreIopq = new CarreTexture("carreIopq", true, false, 176.5f, 62, 12,12);
		this.ajouterObjetGUI(carreIopq, dessin, "emblemes/pccml3");
		Jauge jaugeMembresEnLutte = new Jauge("endroits_emplacement_sympathieEnLutte", true, true, 191, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresEnLutte, dessin);
		Jauge jaugeMembresLigueCommuniste = new Jauge("endroits_emplacement_sympathieLigueCommuniste", true, true, 206, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresLigueCommuniste, dessin);
		CarreTexture carreLigueCommuniste = new CarreTexture("carreLigueCommuniste", true, false, 206.5f, 62, 12,12);
		this.ajouterObjetGUI(carreLigueCommuniste, dessin, "emblemes/pco1");
		Jauge jaugeMembresCis = new Jauge("endroits_emplacement_sympathieCis", true, true, 221, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresCis, dessin);
		Jauge jaugeMembresCisGauche = new Jauge("endroits_emplacement_sympathieCisGauche", true, true, 236, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresCisGauche, dessin);
		Jauge jaugeMembresFlp = new Jauge("endroits_emplacement_sympathieFlp", true, true, 251, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresFlp, dessin);
		Jauge jaugeMembresUnionBolchevique = new Jauge("endroits_emplacement_sympathieUnionBolchevique", true, true, 266, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresUnionBolchevique, dessin);
		Jauge jaugeMembresGmr = new Jauge("endroits_emplacement_sympathieGmr", true, true, 281, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresGmr, dessin);
		Jauge jaugeMembresOrg1 = new Jauge("endroits_emplacement_sympathieOrg1", true, true, 296, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresOrg1, dessin);
		Jauge jaugeMembresOrg2 = new Jauge("endroits_emplacement_sympathieOrg2", true, true, 311, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresOrg2, dessin);
		Jauge jaugeMembresOrg3 = new Jauge("endroits_emplacement_sympathieOrg3", true, true, 326, 75, 13, 45);
		this.ajouterObjetGUI(jaugeMembresOrg3, dessin);
	}

	@Override
	public void update(int dx, int dy, Jeu jeu)
	{
		String emplacement = jeu.getEmplacementActif();
		if(visible)
		{
			Iterator<ObjetGUI> i = contenu.iterator();
			while(i.hasNext())
			{
				ObjetGUI objet = i.next();
				if(objet.getId().startsWith("jauge"))
				{
					((Jauge)objet).setNomValeur("endroits_"+ emplacement + "_" + objet.getId().split("_")[3]);
				}
				else if(objet.getId().startsWith("texte") && !objet.getId().startsWith("texte_fixe"))
				{
					((Texte)objet).setNomValeur("endroits_"+ emplacement + "_" + objet.getId().split("_")[3]);
				}
				else if(objet.getId().equals(nomTextePopNbr))
				{
					((Texte)objet).setTexte(String.valueOf((int) (jeu.getValeur("endroits_" + emplacement + "_population")*10000)));
				}
				
				objet.update(dx, dy, jeu);
			}
		}
	}

}
