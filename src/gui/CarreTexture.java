package gui;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.util.Iterator;

import jeu.Jeu;
import moteurGraphique.Dessinatrice;

public class CarreTexture extends ObjetGUI
{
	public CarreTexture(String id, boolean visible, boolean clickable, float x, float y, float hauteur,
			float largeur)
	{
		super(id, visible, clickable, x, y, hauteur, largeur);
	}

	@Override
	public void dessiner(Dessinatrice dessinatrice)
	{
		if(visible)
		{
			dessinatrice.dessinerGUI(id);
			Iterator<ObjetGUI> i = contenu.iterator();
			while(i.hasNext())
			{
				i.next().dessiner(dessinatrice);
			}
		}
	}

	@Override
	public void update(int dx, int dy, Jeu jeu)
	{
		if(visible)
		{
			Iterator<ObjetGUI> i = contenu.iterator();
			while(i.hasNext())
			{
				i.next().update(dx, dy, jeu);
			}
		}
	}
}
