package jeu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import donnees.DateDonnee;
import donnees.InfosBoiteDeDialogue;
import donnees.updatables.*;
import donnees.xml.XMLReader;

public class Partie
{
	private HashMap<String, ArrayList<Updatable>> listeUpdatable;
	private String idOrganisationJoueuse;
	private LinkedList<InfosBoiteDeDialogue> listeDialogues;
	private InfosBoiteDeDialogue dialogueActif;
	
	private DateDonnee dateDonnee;
	
	public Partie(Jeu jeu)
	{
		dateDonnee = new DateDonnee(1966, 12, 31, 20, 0);
		
		listeUpdatable = new HashMap<String, ArrayList<Updatable>>();
		
		listeUpdatable.put("endroits", new ArrayList<Updatable>());
		listeUpdatable.get("endroits").add(new Province(jeu));
		
		
		listeUpdatable.put("eventshistoriques", XMLReader.getEventsHistoriques());
		
		listeDialogues = new LinkedList<InfosBoiteDeDialogue>();
		
		//test
		listeUpdatable.put("enjeu", new ArrayList<Updatable>());
		listeUpdatable.get("enjeu").add(new Enjeu());
		
		
	}
	
	public void update(Jeu jeu)
	{
		/*
		 * 
		 * */
		
		Iterator<Map.Entry<String, ArrayList<Updatable>>> iterateur = listeUpdatable.entrySet().iterator();
		while(iterateur.hasNext())
		{
			for(Updatable element: iterateur.next().getValue())
			{
				element.update(jeu);
			}
		}		
	}
	
	public float getValeur(String nomValeur)
	{
		float valeur = 0.0f;
		
		if(nomValeur.startsWith("endroits"))
		{
			valeur = listeUpdatable.get("endroits").get(0).getValeur(nomValeur.substring(9));
		}
		else if(nomValeur.contentEquals("annee"))
		{
			valeur = dateDonnee.getAnnee(); 
		}
		else if(nomValeur.contentEquals("mois"))
		{
			valeur = dateDonnee.getMois(); 
		}
		else if(nomValeur.contentEquals("jour"))
		{
			valeur = dateDonnee.getJour(); 
		}
		else if(nomValeur.contentEquals("heure"))
		{
			valeur = dateDonnee.getHeure(); 
		}
		else if(nomValeur.contentEquals("minute"))
		{
			valeur = dateDonnee.getMinute(); 
		}
		else
		{
			String debut = nomValeur.substring(0, nomValeur.indexOf("_"));
			for(Updatable objet: listeUpdatable.get(debut))
			{
				String nomObjet = nomValeur.substring(debut.length()+1);
				if(nomObjet.startsWith(objet.getID()))
				{
					valeur = objet.getValeur(nomObjet.substring(objet.getID().length()+1));
				}
			}
		}
		return valeur;
	}
	
	public String getTexte(String nomValeur)
	{
		String valeur = "";
		
		
		
		if(nomValeur.startsWith("endroits"))
		{
			valeur = listeUpdatable.get("endroits").get(0).getTexte(nomValeur.substring(9));
		}
		else if(nomValeur.contentEquals("annee"))
		{
			valeur = Integer.toString(dateDonnee.getAnnee()); 
		}
		else if(nomValeur.contentEquals("mois"))
		{
			valeur = dateDonnee.getNomMois();
		}
		else if(nomValeur.contentEquals("jour"))
		{
			valeur = Integer.toString(dateDonnee.getJour()); 
		}
		else if(nomValeur.contentEquals("heure"))
		{
			valeur = Integer.toString(dateDonnee.getHeure()); 
		}
		else if(nomValeur.contentEquals("minute"))
		{
			valeur = Integer.toString(dateDonnee.getMinute()); 
		}
		else
		{
			String debut = nomValeur.substring(0, nomValeur.indexOf("_"));
			for(Updatable objet: listeUpdatable.get(debut))
			{
				String nomObjet = nomValeur.substring(debut.length()+1);
				if(nomObjet.startsWith(objet.getID()))
				{
					valeur = objet.getTexte(nomObjet.substring(objet.getID().length()+1));
				}
			}
		}
		
		
		return valeur;
	}
	
	public void augmenterValeur(String nomValeur, float quantite)
	{
		
		String debut = nomValeur.substring(0, nomValeur.indexOf("_"));
		
		if(nomValeur.startsWith("endroits"))
		{
			listeUpdatable.get("endroits").get(0).augmenterValeur(nomValeur.substring(9), quantite);
		}
		else
		{
			for(Updatable objet: listeUpdatable.get(debut))
			{
				String nomObjet = nomValeur.substring(debut.length()+1);
				if(nomObjet.startsWith(objet.getID()))
				{
					objet.augmenterValeur(nomObjet.substring(objet.getID().length()+1), quantite);
				}
			}
		}
		
		
	}
	
	public void setTexte(String nomValeur, String texte)
	{
		String debut = nomValeur.substring(0, nomValeur.indexOf("_"));
		
		for(Updatable objet: listeUpdatable.get(debut))
		{
			String nomObjet = nomValeur.substring(debut.length()+1);
			if(nomObjet.startsWith(objet.getID()))
			{
				objet.setTexte(nomObjet.substring(objet.getID().length()+1), texte);
			}
		}
	}

	public void augmenterMinute(int combien)
	{
		dateDonnee.augmenterMinute(combien);
	}

	public DateDonnee getDateDonnee()
	{
		return dateDonnee;
	}
	
	public InfosBoiteDeDialogue prochainDialogue()
	{
		InfosBoiteDeDialogue infos = null;
		if(listeDialogues.size() > 0)
		{
			infos = listeDialogues.poll();
			dialogueActif = infos;
		}

		return infos;
	}
	
	public void ajouterDialogue(InfosBoiteDeDialogue infos)
	{
		listeDialogues.add(infos);
	}

	public LinkedList<InfosBoiteDeDialogue> getListeDialogues()
	{
		return listeDialogues;
	}

	public InfosBoiteDeDialogue getDialogueActif()
	{
		return dialogueActif;
	}
}
