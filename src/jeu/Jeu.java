package jeu;

import gui.Interface;
import moteurGraphique.Dessinatrice;

import java.util.ArrayList;
import java.util.HashMap;

import donnees.DateDonnee;
import donnees.InfosBoiteDeDialogue;
import donnees.actions.Action;
import donnees.actions.ActionAugmenterValeur;
import donnees.actions.ActionComposite;
import donnees.actions.ActionDialogue;
import donnees.actions.ActionFantoche;
import donnees.actions.ActionFenetreEmplacement;
import donnees.actions.ActionFenetreRegion;
import donnees.actions.ActionFermerFenetre;
import donnees.actions.ActionOuvrirFenetre;
import donnees.actions.ActionProchainDialogue;
import donnees.updatables.EventHistorique;
import donnees.xml.XMLReader;

public class Jeu
{
	private Interface gui;
	private Partie partieActive;
	
	
	private HashMap<String, Action> actionsJoueuse;
	
	private String regionActive;
	private String emplacementActif;
	
	private float millis = 0f;
	
	private int vitesse;
	private boolean pause = true;
	
	public Jeu()
	{
		//Temporaire
		partieActive = new Partie(this);
		vitesse = 1;
		
		
		gui = new Interface();
		actionsJoueuse = new HashMap<String, Action>();
		
		regionActive = "gaspesie";		
		
		
		//Fen�tre r�gion
		actionsJoueuse.put("gaspesie", new ActionFenetreRegion("gaspesie"));
		actionsJoueuse.put("capitaleNationale", new ActionFenetreRegion("capitaleNationale"));
		actionsJoueuse.put("boutonFermerRegion", new ActionFermerFenetre("region"));
		
		//FenetreEmplacement
		actionsJoueuse.put("boutonFermerEmplacement", new ActionFermerFenetre("emplacement"));
		actionsJoueuse.put("carreEmplacement1", new ActionFenetreEmplacement(1));
		actionsJoueuse.put("carreEmplacement2", new ActionFenetreEmplacement(2));
		actionsJoueuse.put("carreEmplacement3", new ActionFenetreEmplacement(3));
		actionsJoueuse.put("carreEmplacement4", new ActionFenetreEmplacement(4));
		
		partieActive.update(this);
		update(1, 0, 0);
	}
	
	public void clic(float x, float y)
	{
		
		System.out.println(gui.clic(x,y)); //debug
		String nomAction = gui.clic(x, y);
		
		
		//on a une liste d'actions clickables qui est class�e par nom d'�l�ment gui
		if(nomAction != "")
		{
			try
			{
				actionsJoueuse.get(gui.clic(x, y)).executer(this);
			} catch (Exception e)
			{
				System.err.println(e);
			}
			
		}
	}
	
	public void update(float delta, float xSouris, float ySouris)
	{
		if(gui.dialogueActifOuvert())
		{
			pause = true;
		}
		if(!pause)
		{
			millis += delta;
			if(millis > 10)
			{
				partieActive.augmenterMinute(vitesse*3);
				millis -= 10;
				partieActive.update(this);
			}
		}
				
		if(partieActive.getListeDialogues().size() > 0 && !gui.dialogueActifOuvert())
		{
			prochainDialogue();
		}
		gui.update(this, xSouris, ySouris);
		gui.dessiner();
	}
	
	public float getValeur(String nomValeur)
	{
		return partieActive.getValeur(nomValeur);
	}
	
	public String getTexte(String nomValeur)
	{
		return partieActive.getTexte(nomValeur);
	}
	
	public void augmenterValeur(String nomValeur, float quantite)
	{
		partieActive.augmenterValeur(nomValeur, quantite);
	}
	
	public void setTexte(String nomTexte, String contenu)
	{
		partieActive.setTexte(nomTexte, contenu);
	}
	
	public void fermerFenetre(String nomFenetre)
	{
		gui.fermerFenetre(nomFenetre);
	}
	
	public void ouvrirFenetre(String nomFenetre)
	{
		gui.ouvrirFenetre(nomFenetre);
	}
	
	public void zoom(float scroll)
	{
		gui.zoom(scroll);
	}
	
	public void enHaut()
	{
		gui.enHaut();
	}
	
	public void enBas()
	{
		gui.enBas();
	}
	
	public void aGauche()
	{
		gui.aGauche();
	}
	
	public void aDroite()
	{
		gui.aDroite();
	}
	
	public void fenetreRegion(String idRegion)
	{
		regionActive = idRegion;
		ouvrirFenetre("region");
	}
	
	public void fenetreEmplacement(int noEmplacement)
	{
		fermerFenetre("region");
		int numero = noEmplacement;
		emplacementActif = getTexte("endroits_" + regionActive + "_emplacementId" + numero);
		ouvrirFenetre("emplacement");
	}
		
	public void finJeu()
	{
		gui.nettoyage();
	}

	public void pauseOuStart()
	{
		if(pause)
		{
			pause = false;
		}
		else
		{
			pause = true;
		}
	}
	
	public void prochainDialogue()
	{
		InfosBoiteDeDialogue infos = partieActive.prochainDialogue();
		if(infos != null)
		{
			gui.nouvelleBoiteDedialogue(infos);
			ArrayList<Action> actionsDialogue1 = new ArrayList<Action>();
			actionsDialogue1.add(partieActive.getDialogueActif().getActionChoix1());
			actionsDialogue1.add(new ActionProchainDialogue());
			ActionComposite actionDialogue1 = new ActionComposite(actionsDialogue1);
			actionsJoueuse.put("carreChoix1Dialogue", actionDialogue1);
			
			ArrayList<Action> actionsDialogue2 = new ArrayList<Action>();
			actionsDialogue2.add(partieActive.getDialogueActif().getActionChoix2());
			actionsDialogue2.add(new ActionProchainDialogue());
			ActionComposite actionDialogue2 = new ActionComposite(actionsDialogue2);
			actionsJoueuse.put("carreChoix2Dialogue", actionDialogue2);
			
			ArrayList<Action> actionsDialogue3 = new ArrayList<Action>();
			actionsDialogue3.add(partieActive.getDialogueActif().getActionChoix3());
			actionsDialogue3.add(new ActionProchainDialogue());
			ActionComposite actionDialogue3 = new ActionComposite(actionsDialogue3);
			actionsJoueuse.put("carreChoix3Dialogue", actionDialogue3);
			
			ArrayList<Action> actionsDialogue4 = new ArrayList<Action>();
			actionsDialogue4.add(partieActive.getDialogueActif().getActionChoix4());
			actionsDialogue4.add(new ActionProchainDialogue());
			ActionComposite actionDialogue4 = new ActionComposite(actionsDialogue4);
			actionsJoueuse.put("carreChoix4Dialogue", actionDialogue4);
		}
		else
		{
			gui.fermerFenetreDialogue();
		}
	}
	
	public void ajouterDialogue(InfosBoiteDeDialogue infos)
	{
		partieActive.ajouterDialogue(infos);
	}
	
	//Getters et setters -----------------------
	
	public String getRegionActive()
	{
		return regionActive;
	}

	public String getEmplacementActif()
	{
		return emplacementActif;
	}
	
	public DateDonnee getDateDonnee()
	{
		return partieActive.getDateDonnee();
	}	
}
