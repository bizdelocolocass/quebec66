package carte;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import moteurGraphique.OBJLoader;
import moteurGraphique.Vec3;

public class LoadTrianglesRegion
{

	public static Triangle[] loadOBJModel(String filename)
	{
		/**float[] valeursTemp = OBJLoader.minMaxRegions();
		float minX = valeursTemp[0];
		float maxX = valeursTemp[1];
		float minY = valeursTemp[2];
		float maxY = valeursTemp[3];**/
		
		float minX = OBJLoader.minX;
		float maxX = OBJLoader.maxX;
		float minY = OBJLoader.minY;
		float maxY = OBJLoader.maxY;
		
		FileReader fr = null;
		try
		{
			fr = new FileReader(new File("res/regions/" + filename + ".obj"));
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader reader = new BufferedReader(fr);
			
		String line;
		List<Point> vertices = new ArrayList<Point>();
		ArrayList<Triangle> triangles = new ArrayList<Triangle>();
		
		try
		{
			while(true)
			{
				line = reader.readLine();
				String [] currentLine = line.split(" ");
				if(line.startsWith("v "))
				{
					Point vertex = new Point(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[3]));
					vertices.add(vertex);
				}
				else if(line.startsWith("f "))
				{
					break;
				}
			}
			
			while(line != null)
			{
				if(!line.startsWith("f "))
				{
					line = reader.readLine();
					continue;
				}
				
				String[] currentLine = line.split(" ");
				String[] vertex1 = currentLine[1].split("/");
				String[] vertex2 = currentLine[2].split("/");
				String[] vertex3 = currentLine[3].split("/");
				
				float largeurTemp = maxX-minX;
				float hauteurTemp = maxY-minY;
				
				float x1 = (vertices.get(Integer.parseInt(vertex1[0])-1).getX() - minX)/largeurTemp*400;
				float x2 = (vertices.get(Integer.parseInt(vertex2[0])-1).getX() - minX)/largeurTemp*400;
				float x3 = (vertices.get(Integer.parseInt(vertex3[0])-1).getX() - minX)/largeurTemp*400;
				
				float y1 = 300-(vertices.get(Integer.parseInt(vertex1[0])-1).getY() - minY)/hauteurTemp*300;
				float y2 = 300-(vertices.get(Integer.parseInt(vertex2[0])-1).getY() - minY)/hauteurTemp*300;
				float y3 = 300-(vertices.get(Integer.parseInt(vertex3[0])-1).getY() - minY)/hauteurTemp*300;
				
				
				/**float x1 = (vertices.get(Integer.parseInt(vertex1[0])).getX() - minX)/largeurTemp*2-1;
				float y1 = -(vertices.get(Integer.parseInt(vertex1[0])).getY()-minY/hauteurTemp*2)+1;
				float x2 = (vertices.get(Integer.parseInt(vertex2[0])).getX() - minX)/largeurTemp*2-1;
				float y2 = -(vertices.get(Integer.parseInt(vertex2[0])).getY()-minY/hauteurTemp*2)+1;
				float x3 = (vertices.get(Integer.parseInt(vertex3[0])).getX() - minX)/largeurTemp*2-1;
				float y3 = -(vertices.get(Integer.parseInt(vertex3[0])).getY()-minY/hauteurTemp*2)+1;**/
				
				triangles.add(new Triangle(new Point(x1,y1), new Point(x2,y2), new Point(x3,y3)));
				
				
				
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		Triangle[] triangleArray = new Triangle[triangles.size()];
		int i = 0;
		for(Triangle triangle: triangles)
		{
			triangleArray[i++] = triangle;
		}
		return triangleArray;
		
		
	}

}
