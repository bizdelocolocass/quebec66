package carte;

public class Region
{
	private String id;
	private Triangle[] triangles;
	private float r;
	private float g;
	private float b;
	
	
	public Region(String id)
	{
		this.id = id;
		triangles = LoadTrianglesRegion.loadOBJModel(id);
	}
	
	public void update(int settingCarte, String idHover)
	{
		r = 1f;
		g = 1f;
		b = 1f;
		
		if(idHover == id)
		{
			r -= 0.5f;
		}
	}

	public String getId()
	{
		return id;
	}
	
	public String clic(float clicX, float clicY, float zoom, float dx, float dy)
	{
		String retour = "";
		for(Triangle triangle: triangles)
		{
			if(pointInTriangle(new Point(clicX-200*dx*zoom,clicY-150*dy*zoom), new Point((triangle.getVertex1().getX()-200)*zoom+200,(triangle.getVertex1().getY()-150)*zoom+150), new Point((triangle.getVertex2().getX()-200)*zoom+200,(triangle.getVertex2().getY()-150)*zoom+150), new Point((triangle.getVertex3().getX()-200)*zoom+200,(triangle.getVertex3().getY()-150)*zoom+150)))
			{
				retour = id;
				break;
			}
		}
		
		return retour;
	}
	
	private float sign(Point p1, Point p2, Point p3)
	{
	    return (p1.getX() - p3.getX()) * (p2.getY() - p3.getY()) - (p2.getX() - p3.getX()) * (p1.getY() - p3.getY());
	}

	private boolean pointInTriangle(Point pt, Point v1, Point v2, Point v3)
	{
	    float d1, d2, d3;
	    boolean has_neg, has_pos;

	    d1 = sign(pt, v1, v2);
	    d2 = sign(pt, v2, v3);
	    d3 = sign(pt, v3, v1);

	    has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	    has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	    return !(has_neg && has_pos);
	}

	public Triangle[] getTriangles()
	{
		return triangles;
	}

	public float getR()
	{
		return r;
	}

	public float getG()
	{
		return g;
	}

	public float getB()
	{
		return b;
	}
	
	
}
