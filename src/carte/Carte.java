package carte;

import java.util.ArrayList;

import moteurGraphique.Dessinatrice;

public class Carte
{
	private ArrayList<Region> regions = new ArrayList<Region>();
	private float zoom = 0.8f;
	private float dx = -0.3f;
	private float dy = -0.1f;
	
	private int settingCarte = 0;
	
	public Carte()
	{
	}
	
	public void update(String idHover)
	{
		for(Region region: regions)
		{
			region.update(settingCarte, idHover);
		}
	}
	
	public float getZoom()
	{
		return zoom;
	}
	public float getDx()
	{
		return dx;
	}
	public float getDy()
	{
		return dy;
	}
	
	public void ajouterRegion(String nom, Dessinatrice dessinatrice)
	{
		regions.add(new Region(nom));
		dessinatrice.ajouterRegion(nom);
	}
	
	public void dessiner(Dessinatrice dessinatrice)
	{
		for(Region region: regions)
		{
			dessinatrice.dessinerRegion(region.getId(), region.getR(), region.getG(), region.getB());
		}
	}
	
	public String clic(float clicX, float clicY) 
	{
		String retour = "";
		for(Region region: regions)
		{
			String regionClic = region.clic(clicX, clicY, zoom, dx, dy);
			if(regionClic != "")
			{
				retour = regionClic;
				break;
			}
		}
		return retour;
	}
	
	public void augmenterZoom(float combien) 
	{
		zoom += combien/5;
		if(zoom < 0.6)
		{
			zoom = 0.6f;
		}
		else if(zoom > 2f)
		{
			zoom = 2f;
		}
	}
	
	public void enHaut()
	{
		if(dy > -1f)
		{
			dy -= 0.2/zoom;	
		}
	}
	
	public void enBas()
	{
		if(dy < 1f)
		{
			dy += 0.2/zoom;	
		}
		
	}
	
	public void aGauche()
	{
		if(dx < 1f)
		{
			dx += 0.2/zoom;
		}		
	}
	
	public void aDroite()
	{
		if(dx > -1.2f)
		{
			dx -= 0.2/zoom;
		}
	}
}
