package donnees.valeurs;

import jeu.Jeu;

public abstract class Valeur
{
	public abstract float resultat(Jeu jeu);
}
