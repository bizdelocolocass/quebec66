package donnees.valeurs;

import jeu.Jeu;

public class ValeurConstante extends Valeur
{
	private float valeur;

	public ValeurConstante(float valeur)
	{
		this.valeur = valeur;
	}

	@Override
	public float resultat(Jeu jeu)
	{
		return valeur;
	}

}
