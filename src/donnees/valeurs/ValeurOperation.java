package donnees.valeurs;

import jeu.Jeu;

public class ValeurOperation extends Valeur
{

	private Valeur valeurGauche;
	private Valeur valeurDroite;
	private String operateur;
	


	public ValeurOperation(Valeur valeurGauche, String operateur, Valeur valeurDroite)
	{
		this.valeurGauche = valeurGauche;
		this.valeurDroite = valeurDroite;
		this.operateur = operateur;
	}



	public float resultat(Jeu jeu)
	{
		float total = 0.0f;

		if(operateur == "plus")
		{
			total = valeurGauche.resultat(jeu) + valeurDroite.resultat(jeu);
		}
		else if(operateur == "moins")
		{
			total = valeurGauche.resultat(jeu) - valeurDroite.resultat(jeu);
		}
		else if(operateur == "multiplie")
		{
			total = valeurGauche.resultat(jeu) * valeurDroite.resultat(jeu);
		}
		else if(operateur == "divise")
		{
			total = valeurGauche.resultat(jeu) / valeurDroite.resultat(jeu);
		}
		
		return total;
	}
}
