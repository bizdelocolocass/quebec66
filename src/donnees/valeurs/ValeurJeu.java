package donnees.valeurs;

import jeu.Jeu;

public class ValeurJeu extends Valeur
{
protected String nomValeur;
	
	public ValeurJeu(String nomValeur)
	{
		this.nomValeur = nomValeur;
	}

	public float resultat(Jeu jeu)
	{
		return jeu.getValeur(nomValeur);
	}
}
