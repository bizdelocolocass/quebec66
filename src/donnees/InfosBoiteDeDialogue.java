package donnees;

import donnees.actions.Action;

public class InfosBoiteDeDialogue
{
	private String texteChoix1;
	private Action actionChoix1;
	
	private String texteChoix2;
	private Action actionChoix2;
	
	private String texteChoix3;
	private Action actionChoix3;
	
	private String texteChoix4;
	private Action actionChoix4;
	
	private String nomImage;
	private String textePrincipal;
	private String titre;
	
	public InfosBoiteDeDialogue(String texteChoix1, Action actionChoix1, String texteChoix2, Action actionChoix2,
			String texteChoix3, Action actionChoix3, String texteChoix4, Action actionChoix4, String titre, String nomImage,
			String textePrincipal)
	{
		this.texteChoix1 = texteChoix1;
		this.actionChoix1 = actionChoix1;
		this.texteChoix2 = texteChoix2;
		this.actionChoix2 = actionChoix2;
		this.texteChoix3 = texteChoix3;
		this.actionChoix3 = actionChoix3;
		this.texteChoix4 = texteChoix4;
		this.actionChoix4 = actionChoix4;
		this.titre = titre;
		this.nomImage = nomImage;
		this.textePrincipal = textePrincipal;
	}

	public String getTexteChoix1()
	{
		return texteChoix1;
	}

	public Action getActionChoix1()
	{
		return actionChoix1;
	}

	public String getTexteChoix2()
	{
		return texteChoix2;
	}

	public Action getActionChoix2()
	{
		return actionChoix2;
	}

	public String getTexteChoix3()
	{
		return texteChoix3;
	}

	public Action getActionChoix3()
	{
		return actionChoix3;
	}

	public String getTexteChoix4()
	{
		return texteChoix4;
	}

	public Action getActionChoix4()
	{
		return actionChoix4;
	}

	public String getNomImage()
	{
		return nomImage;
	}

	public String getTextePrincipal()
	{
		return textePrincipal;
	}

	public String getTitre()
	{
		return titre;
	}
	


}
