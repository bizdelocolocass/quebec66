package donnees.updatables;

import jeu.Jeu;

public class Emplacement extends Updatable
{

	public Emplacement(String id)
	{
		super();
		this.id = id;
	}

	@Override
	public void update(Jeu jeu)
	{
		
	}
	
	@Override
	public float getValeur(String nomValeur)
	{
		String nomVariable = nomValeur.split("_")[1];
		return this.valeurs.get(nomVariable);
	}

	@Override
	public String getTexte(String nomValeur)
	{
		if(nomValeur.startsWith(id))
		{
			String nomVariable = nomValeur.split("_")[1];
		return this.textes.get(nomVariable);
		}
		else
		{
			return "";
		}
	}
}
