package donnees.updatables;

import java.util.ArrayList;

import donnees.DateDonnee;
import donnees.actions.Action;
import donnees.conditions.Condition;
import donnees.influences.Influence;
import jeu.Jeu;

public class EventHistorique extends Updatable
{	
	private Condition condition;
	private ArrayList<Influence> influences;
	private DateDonnee dateMin;
	private DateDonnee dateMax;
	private int heure;
	private Action action;
	
	private boolean triggered = false;
	
	
	

	public EventHistorique(Condition condition, ArrayList<Influence> influences, DateDonnee dateMin, DateDonnee dateMax, int heure, Action action)
	{
		super();
		this.condition = condition;
		this.influences = influences;
		this.dateMin = dateMin;
		this.dateMax = dateMax;
		this.heure = heure;
		this.action = action;
	}

	@Override
	public void update(Jeu jeu)
	{	
		if(jeu.getDateDonnee().isChangementHeure())
		{
			int intervaleJours = DateDonnee.differenceDatesEnJour(dateMax, dateMin) + 1;
			
	 		
			
			//ajouter condition heure
			if(!triggered && this.heure == jeu.getValeur("heure") && condition.tester(jeu) && (DateDonnee.differenceDatesEnJour(jeu.getDateDonnee(), dateMin) >= 0 && DateDonnee.differenceDatesEnJour(dateMax, jeu.getDateDonnee()) >= 0))
			{
				float chances = 1.0f/intervaleJours; //multipliť par influences
				double hasard = Math.random();
				
				System.out.println(hasard);
				if(hasard < chances)
				{
					executer(jeu);
					triggered = true;
				}
				
			}
		}
	}
	
	public void executer(Jeu jeu)
	{
		action.executer(jeu);
	}

}
