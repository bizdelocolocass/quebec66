package donnees.updatables;

import java.util.*;

import jeu.Jeu;

public abstract class Updatable
{
	protected String id;
	protected HashMap<String, Float> valeurs;
	public HashMap<String, Float> getValeurs()
	{
		return valeurs;
	}

	protected HashMap<String, String> textes;

	public Updatable()
	{
		valeurs = new HashMap<String, Float>();
		textes = new HashMap<String, String>();
	}
	
	public String getID()
	{
		return id;
	}

	public void setNom(String nom)
	{
		this.id = nom;
	}

	public String getTexte(String nomValeur)
	{
		return textes.get(nomValeur);
	}
	
	public void setTexte(String nomTexte, String contenu)
	{
		textes.put(nomTexte, contenu);
	}
	
	public float getValeur(String nomValeur)
	{
		return valeurs.get(nomValeur);
	}
	
	public void augmenterValeur(String nomValeur, float quantite)
	{
		float quantiteTemp =  valeurs.get(nomValeur) + quantite;
		if(quantiteTemp > 100)
		{
			quantiteTemp = 100;
		}
		else if (quantiteTemp < 0)
		{
			quantiteTemp = 0;
		}
		valeurs.put(nomValeur, quantiteTemp);
	}
	
	public void putValeur(String nom, float quantite)
	{
		valeurs.put(nom, quantite);
	}
	
	public void putTexte(String nom, String texte)
	{
		textes.put(nom, texte);
	}
	
	public abstract void update(Jeu jeu);
}
