 package donnees.updatables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import donnees.xml.XMLReader;
import jeu.Jeu;

public class Province extends Updatable
{
	ArrayList<RegionDonnee> regions;
	
	public Province(Jeu jeu)
	{
		super();
		id = "province";
		regions = XMLReader.getRegions();
		update(jeu);
	}

	
	@Override
	public void update(Jeu jeu)
	{
		HashMap<String, Float> mapTemp = new HashMap<String, Float>();

		mapTemp.put("population", 0.0f);
		for(RegionDonnee emplacement: regions)
		{
			emplacement.update(jeu);
			mapTemp.put("population", emplacement.getValeur(emplacement.getID() + "_population")+mapTemp.get("population"));
			this.valeurs.put("population", mapTemp.get("population"));
		}
		
		for(RegionDonnee emplacement: regions)
		{
			Iterator<Map.Entry<String, Float>> itr = emplacement.getValeurs().entrySet().iterator();
			while(itr.hasNext())
	        {
				Map.Entry<String, Float> entry = itr.next();
				if(!entry.getKey().endsWith("population"))
				{
					float ratioPop = emplacement.getValeur(emplacement.getID() + "_population")/getValeur(id + "_population");
					if(mapTemp.containsKey(entry.getKey()))
		            {
						float add = 0.0f;
						add = entry.getValue()*ratioPop;
						add += mapTemp.get(entry.getKey());
	            		mapTemp.put(entry.getKey(), add);
		            }
					else
					{
						 mapTemp.put(entry.getKey(), entry.getValue()*ratioPop);
					}
				}
	        }
		}
		this.valeurs = mapTemp;
	}

	@Override
	public float getValeur(String nomValeur)
	{
		float valeur = 0f;
		
		String nomEndroit = nomValeur.split("_")[0];
		String nomVariable = nomValeur.split("_")[1];
		
		// checker si debut de la string ,donc id region c'est nous, puis si c'est dans enfants
		if(nomEndroit.equals("province"))
		{
			valeur = valeurs.get(nomVariable);
		}
		else
		{
			for(RegionDonnee region: regions)
			{
				valeur = region.getValeur(nomValeur);
				if(valeur != 0f)
				{
					break;
				}
			}
		}
		return valeur;
	}
	
	@Override
	public String getTexte(String nomValeur)
	{
		String valeur = "";
		
		String nomEndroit = nomValeur.split("_")[0];
		String nomVariable = nomValeur.split("_")[1];
		// checker si debut de la string ,donc id region c'est nous, puis si c'est dans enfants
		if(nomEndroit == "province")
		{
			
			valeur = textes.get(nomVariable);
		}
		else
		{
			for(RegionDonnee region: regions)
			{
				valeur = region.getTexte(nomValeur);
				if(valeur != "")
				{
					break;
				}
			}
		}
		return valeur;
	}
	
	@Override
	public void augmenterValeur(String nomValeur, float quantite)
	{	
		for(RegionDonnee region: regions)
		{
				region.augmenterValeur(nomValeur, quantite);
		}
	}
}
