package donnees.updatables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import jeu.Jeu;

public class RegionDonnee extends Updatable
{
	private ArrayList<Emplacement> emplacements;
	
	public RegionDonnee(String idRegion)
	{
		super();
		this.id = idRegion;
		emplacements = new ArrayList<Emplacement>();
	}

	@Override
	public void update(Jeu jeu)
	{
		HashMap<String, Float> mapTemp = new HashMap<String, Float>();
		
		mapTemp.put("population", 0.0f);
		for(Emplacement emplacement: emplacements)
		{
			emplacement.update(jeu);
			mapTemp.put("population", emplacement.getValeur(emplacement.getID() + "_population")+mapTemp.get("population"));
		}
		
		for(Emplacement emplacement: emplacements)
		{
			Iterator<Map.Entry<String, Float>> itr = emplacement.getValeurs().entrySet().iterator();
			while(itr.hasNext())
	        {
				Map.Entry<String, Float> entry = itr.next();
				if(!entry.getKey().endsWith("population"))
				{
					float ratioPop = emplacement.getValeur(emplacement.getID() + "_population")/getValeur(id + "_population");
					if(mapTemp.containsKey(entry.getKey()))
		            {
						float add = 0.0f;
						add = entry.getValue()*ratioPop;
						add += mapTemp.get(entry.getKey());
	            		mapTemp.put(entry.getKey(), add);
		            }
					else
					{
						 mapTemp.put(entry.getKey(), entry.getValue()*ratioPop);
					}
				}
	        }
		}
		this.valeurs = mapTemp;
	}

	public void ajouterEmplacement(Emplacement place)
	{
		emplacements.add(place);
	}
	
	@Override
	public float getValeur(String nomValeur)
	{
		float valeur = 0f;
		
		String nomEndroit = nomValeur.split("_")[0];
		String nomVariable = nomValeur.split("_")[1];
		
		// checker si debut de la string ,donc id region c'est nous, puis si c'est dans enfants
		if(nomEndroit.equals(id))
		{
			try
			{
				valeur = valeurs.get(nomVariable);
			} catch (Exception e)
			{
			}
		}
		else
		{
			for(Emplacement place: emplacements)
			{
				if(nomEndroit.equals(place.id))
				{
					valeur = place.getValeur(nomValeur);
					if(valeur != 0f)
					{
						break;
					}
				}
			}
		}
		// checker si debut de la string ,donc id region c'est nous, puis si c'est dans enfants
		return valeur;
	}
	
	@Override
	public String getTexte(String nomValeur)
	{
		String valeur = "";
		
		String nomEndroit = nomValeur.split("_")[0];
		String nomVariable = nomValeur.split("_")[1];
		// checker si debut de la string ,donc id region c'est nous, puis si c'est dans enfants
		if(nomEndroit.equals(id))
		{
			if(nomVariable.startsWith("emplacementId"))
			{
				if(nomVariable.equals("emplacementId1"))
				{
					return emplacements.get(0).getID();
				}
				else if(nomVariable.equals("emplacementId2"))
				{
					return emplacements.get(1).getID();
				}
				else if(nomVariable.equals("emplacementId3"))
				{
					return emplacements.get(2).getID();
				}
				else if(nomVariable.equals("emplacementId4"))
				{
					return emplacements.get(3).getID();
				}
			}
			else if(nomVariable.startsWith("emplacement"))
			{
				if(nomVariable.equals("emplacement1"))
				{
					return emplacements.get(0).getTexte(emplacements.get(0).getID() + "_nom");
				}
				else if(nomVariable.equals("emplacement2"))
				{
					return emplacements.get(1).getTexte(emplacements.get(1).getID() + "_nom");
				}
				else if(nomVariable.equals("emplacement3"))
				{
					return emplacements.get(2).getTexte(emplacements.get(2).getID() + "_nom");
				}
				else if(nomVariable.equals("emplacement4"))
				{
					return emplacements.get(3).getTexte(emplacements.get(3).getID() + "_nom");
				}
			}
			else
			{
				
				try
				{
					valeur = textes.get(nomVariable);
				} catch (Exception e)
				{
				}
			}
			
		}
		else
		{
			for(Emplacement place: emplacements)
			{
				valeur = place.getTexte(nomValeur);
				if(valeur != "")
				{
					break;
				}
			}
		}
		// checker si debut de la string ,donc id region c'est nous, puis si c'est dans enfants
		return valeur;
	}
	
	@Override
	public void augmenterValeur(String nomValeur, float quantite)
	{		
		String nomEndroit = nomValeur.split("_")[0];
		String nomVariable = nomValeur.split("_")[1];
		// checker si debut de la string ,donc id region c'est nous, puis si c'est dans enfants
		
		for(Emplacement emplacement: emplacements)
		{
			if(nomEndroit.equals(emplacement.getID()))
			{
				emplacement.augmenterValeur(nomVariable, quantite);
				break;
			}
		}
	}
}
