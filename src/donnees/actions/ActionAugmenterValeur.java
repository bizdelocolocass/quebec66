package donnees.actions;


import donnees.conditions.Condition;
import jeu.Jeu;

public class ActionAugmenterValeur extends Action
{
	private String nomValeur;
	private float quantite;
	
	
	public ActionAugmenterValeur(String nomValeur, float quantite)
	{
		this.nomValeur = nomValeur;
		this.quantite = quantite;
	}
	
	public ActionAugmenterValeur(String nomValeur, float quantite, Condition condition)
	{
		this.nomValeur = nomValeur;
		this.quantite = quantite;
		this.condition = condition;
	}

	@Override
	public void executer(Jeu jeu)
	{
		if(condition.tester(jeu))
		{
			jeu.augmenterValeur(nomValeur, quantite);
		}
	}

}
