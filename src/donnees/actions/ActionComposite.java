package donnees.actions;

import java.util.ArrayList;

import jeu.Jeu;


public class ActionComposite extends Action
{
	private ArrayList<Action> listeActions;

	public ActionComposite(ArrayList<Action> listeActions)
	{
		this.listeActions = listeActions;
	}

	@Override
	public void executer(Jeu jeu)
	{
		for(Action actionTemp: listeActions)
		{
			actionTemp.executer(jeu);
		}
	}

}
