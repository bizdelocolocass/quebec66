package donnees.actions;

import donnees.InfosBoiteDeDialogue;
import jeu.Jeu;

public class ActionDialogue extends Action
{
	private InfosBoiteDeDialogue infos;

	public ActionDialogue(InfosBoiteDeDialogue infos)
	{
		this.infos = infos;
	}

	@Override
	public void executer(Jeu jeu)
	{
		jeu.ajouterDialogue(infos);
	}

}
