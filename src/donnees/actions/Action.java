package donnees.actions;

import donnees.conditions.Condition;
import donnees.conditions.ConditionFantoche;
import jeu.Jeu;

public abstract class Action
{

	protected Condition condition;
	
	public Action()
	{
		condition = new ConditionFantoche();
		// TODO Auto-generated constructor stub
	}

	public void setCondition(Condition condition)
	{
		this.condition = condition;
	}

	public abstract void executer(Jeu jeu);
}
