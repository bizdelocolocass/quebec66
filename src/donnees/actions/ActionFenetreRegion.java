package donnees.actions;

import jeu.Jeu;

public class ActionFenetreRegion extends Action
{
	private String idRegion;

	public ActionFenetreRegion(String idRegion)
	{
		super();
		this.idRegion = idRegion;
	}

	@Override
	public void executer(Jeu jeu)
	{
		jeu.fenetreRegion(idRegion);
	}

}
