package donnees.actions;

import jeu.Jeu;

public class ActionFermerFenetre extends Action
{
	String idFenetre = "";
	
	public ActionFermerFenetre(String idFenetre)
	{
		this.idFenetre = idFenetre;
	}

	@Override
	public void executer(Jeu jeu)
	{
		jeu.fermerFenetre(idFenetre);
	}
}
