package donnees.actions;

import jeu.Jeu;

public class ActionFenetreEmplacement extends Action
{
	private int idEmplacement;

	public ActionFenetreEmplacement(int noEmplacement)
	{
		super();
		this.idEmplacement = noEmplacement;
	}

	@Override
	public void executer(Jeu jeu)
	{
		jeu.fenetreEmplacement(idEmplacement);
	}

}
