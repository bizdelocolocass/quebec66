package donnees.actions;

import jeu.Jeu;

public class ActionProchainDialogue extends Action
{

	public ActionProchainDialogue()
	{
	}

	@Override
	public void executer(Jeu jeu)
	{
		jeu.prochainDialogue();
	}

}
