package donnees.actions;

import jeu.Jeu;

public class ActionOuvrirFenetre extends Action
{
	private String idFenetre = "";

	public ActionOuvrirFenetre(String idFenetre)
	{
		this.idFenetre = idFenetre;
	}

	@Override
	public void executer(Jeu jeu)
	{
		jeu.ouvrirFenetre(idFenetre);
	}

}
