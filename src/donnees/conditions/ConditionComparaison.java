package donnees.conditions;

import donnees.valeurs.Valeur;
import jeu.Jeu;

public class ConditionComparaison extends Condition
{
	private Valeur valeurGauche;
	private Valeur valeurDroite;
	private String comparateur;

	/**
	 * 
	 * @param valeurGauche
	 * @param comparateur Soit �egal�, �plus�, �moins�, �plusEgal� et �moinsEgal�
	 * @param valeurDroite
	 */
	public ConditionComparaison(Valeur valeurGauche, String comparateur, Valeur valeurDroite)
	{
		this.valeurGauche = valeurGauche;
		this.valeurDroite = valeurDroite;
		this.comparateur = comparateur;
	}

	@Override
	public boolean tester(Jeu jeu)
	{
		boolean retour = false;
		switch (comparateur)
		{
			case "egal":
			{
				retour = valeurGauche.resultat(jeu) == valeurDroite.resultat(jeu);
				break;
			}
			case "plus":
			{
				retour = valeurGauche.resultat(jeu) > valeurDroite.resultat(jeu);
				break;
			}
			case "moins":
			{
				retour = valeurGauche.resultat(jeu) < valeurDroite.resultat(jeu);
				break;
			}
			case "plusEgal":
			{
				retour = valeurGauche.resultat(jeu) >= valeurDroite.resultat(jeu);
				break;
			}
			case "moinsEgal":
			{
				retour = valeurGauche.resultat(jeu) <= valeurDroite.resultat(jeu);
				break;
			}
			default:
			{
				throw new IllegalArgumentException("Unexpected value: " + comparateur);
			}
		}
		return retour;
	}

}
