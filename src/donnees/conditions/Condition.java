package donnees.conditions;

import jeu.Jeu;

public abstract class Condition
{
	public Condition()
	{
		// TODO Auto-generated constructor stub
	}

	public abstract boolean tester(Jeu jeu);
}
