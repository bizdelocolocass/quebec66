package donnees.conditions;

import java.util.ArrayList;

import jeu.Jeu;

public class ConditionComposite extends Condition
{
	ArrayList<Condition> conditions;

	public ConditionComposite(ArrayList<Condition> conditions)
	{
		this.conditions = conditions;
	}

	@Override
	public boolean tester(Jeu jeu)
	{
		boolean check = true;
		for(Condition conditionTemp: conditions)
		{
			if(!conditionTemp.tester(jeu))
			{
				check = false;
			}		
		}
		return check;
	}

}
