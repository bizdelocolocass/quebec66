package donnees.influences;

import donnees.DateDonnee;

public class Influence
{
	private String nomValeur;
	private float borneInferieure;
	private float borneSuperieure;
	private boolean croissant;
	
	public Influence(String nomValeur, float borneInferieure, float borneSuperieure, boolean croissant)
	{
		super();
		this.nomValeur = nomValeur;
		this.borneInferieure = borneInferieure;
		this.borneSuperieure = borneSuperieure;
		this.croissant = croissant;
	}	
	
}
