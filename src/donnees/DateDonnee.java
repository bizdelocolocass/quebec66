package donnees;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateDonnee
{
	private int annee;
	private int mois;
	private int jour;
	private int heure;
	private int minute;

	private boolean changementHeure = false;

	public DateDonnee(int annee, int mois, int jour, int heure, int minute)
	{
		super();
		this.annee = annee;
		this.mois = mois;
		this.jour = jour;
		this.heure = heure;
		this.minute = minute;
	}

	public void augmenterAnnee()
	{
		annee++;
	}
	
	public void augmenterMois()
	{
		if(mois < 12)
		{
			mois++;
		}
		else
		{
			mois = 1;
			augmenterAnnee();
		}
	}
	
	public void augmenterJour()
	{
		if(mois == 1 || mois == 3 || mois == 5 || mois == 7 || mois == 8 || mois == 10 || mois == 12)
		{
			if(jour < 31)
			{
				jour++;
			}
			else
			{
				jour = 1;
				augmenterMois();
			}
		}
		else if(mois == 4 || mois == 6 || mois == 9 || mois == 11)
		{
			if(jour < 30)
			{
				jour++;
			}
			else
			{
				jour = 1;
				augmenterMois();
			}
		}
		else if(mois == 2)
		{
			if(annee % 4 == 0)
			{
				if(jour < 29)
				{
					jour++;
				}
				else
				{
					jour = 1;
					augmenterMois();
				}
			}
			else
			{
				if(jour < 28)
				{
					jour++;
				}
				else
				{
					jour = 1;
					augmenterMois();
				}
			}
		}
	}
	
	public void augmenterHeure()
	{
		changementHeure = true;
		if(heure < 23)
		{
			heure++;
		}
		else
		{
			heure = 0;
			augmenterJour();
		}
	}
	
	public void augmenterMinute(int combien)
	{
		changementHeure = false;
		if(minute < 59)
		{
			minute += combien;
		}
		else
		{
			minute = 0;
			augmenterHeure();
		}
	}

	public int getAnnee()
	{
		return annee;
	}

	public int getMois()
	{
		return mois;
	}

	public int getJour()
	{
		return jour;
	}

	public int getHeure()
	{
		return heure;
	}

	public int getMinute()
	{
		return minute;
	}
	
	public String getNomMois()
	{
		String valeur = "";
		if(mois == 1)
		{
			valeur = "jan";
		}
		else if(mois == 2)
		{
			valeur = "fev";
		}
		else if(mois == 3)
		{
			valeur = "mar";
		}
		else if(mois == 4)
		{
			valeur = "avr";
		}
		else if(mois == 5)
		{
			valeur = "mai";
		}
		else if(mois == 6)
		{
			valeur = "jun";
		}
		else if(mois == 7)
		{
			valeur = "jul";
		}
		else if(mois == 8)
		{
			valeur = "aou";
		}
		else if(mois == 9)
		{
			valeur = "sep";
		}
		else if(mois == 10)
		{
			valeur = "oct";
		}
		else if(mois == 11)
		{
			valeur = "nov";
		}
		else if(mois == 12)
		{
			valeur = "dec";
		}
		
		return valeur;
	}
	
	public static int differenceDatesEnJour(DateDonnee plusGrandeDate, DateDonnee plusPetiteDate)
	{
		LocalDate dateGrande = LocalDate.of(plusGrandeDate.getAnnee(), plusGrandeDate.getMois(), plusGrandeDate.getJour());
		LocalDate datePetite= LocalDate.of(plusPetiteDate.getAnnee(), plusPetiteDate.getMois(), plusPetiteDate.getJour());
		long days = ChronoUnit.DAYS.between(datePetite, dateGrande);
		return (int) days;
	}

	public boolean isChangementHeure()
	{
		return changementHeure;
	}
}
