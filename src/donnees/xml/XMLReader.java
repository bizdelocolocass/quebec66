package donnees.xml;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import donnees.DateDonnee;
import donnees.InfosBoiteDeDialogue;
import donnees.actions.Action;
import donnees.actions.ActionAugmenterValeur;
import donnees.actions.ActionDialogue;
import donnees.actions.ActionFantoche;
import donnees.conditions.Condition;
import donnees.conditions.ConditionComparaison;
import donnees.conditions.ConditionFantoche;
import donnees.influences.Influence;
import donnees.updatables.Emplacement;
import donnees.updatables.EventHistorique;
import donnees.updatables.RegionDonnee;
import donnees.updatables.Updatable;
import donnees.valeurs.Valeur;
import donnees.valeurs.ValeurConstante;
import donnees.valeurs.ValeurJeu;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * 
 * @author https://mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
 *
 */
public class XMLReader {

	
	//Faut ajouter les textes
	public static ArrayList<RegionDonnee> getRegions()
	{
		ArrayList<RegionDonnee> listeRegions = new ArrayList<RegionDonnee>();
		try
		{
			File fXmlFile = new File("res/xml/endroits.xml");
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder;
			
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();	  
			
			 NodeList regions = doc.getElementsByTagName("region");
			 for (int temp = 0; temp < regions.getLength(); temp++) 
			    {
			        Node nNode = regions.item(temp);
			                
			                
			        if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			        {
			        	
			            Element eElement = (Element) nNode;
			            RegionDonnee regionTemp = new RegionDonnee(eElement.getAttribute("id"));
			                        
			            regionTemp.putTexte(((Element) eElement.getElementsByTagName("texte").item(0)).getAttribute("id"), eElement.getElementsByTagName("texte").item(0).getTextContent());
			            
					    NodeList villesTemp = eElement.getElementsByTagName("ville");
					    for(int i = 0; i < villesTemp.getLength(); i++)
					    {
					    	Node nodeVille = villesTemp.item(i);
					    	if(nodeVille.getNodeType() == Node.ELEMENT_NODE)
					    	{
					    		Element villeTemp = (Element) nodeVille;
					    		Emplacement place = new Emplacement(villeTemp.getAttribute("id"));
					    		place.putTexte("nom", villeTemp.getElementsByTagName("texte").item(0).getTextContent());
					    		NodeList valeursTemp = villeTemp.getElementsByTagName("valeur");
					    		for(int w = 0; w < valeursTemp.getLength(); w++)
					    		{
					    			Node nodeValeur = valeursTemp.item(w);
					    			if(nodeValeur.getNodeType() == Node.ELEMENT_NODE)
					    			{
					    				float valeurTemp = 0f;
					    				if( nodeValeur.getTextContent() != "")
					    				{
					    					valeurTemp = Float.parseFloat( nodeValeur.getTextContent());
					    				}
					    				place.putValeur(((Element) nodeValeur).getAttribute("id"), valeurTemp);
					    			}
					    		}
					    		regionTemp.ajouterEmplacement(place);
					    	}
					    }
			            listeRegions.add(regionTemp);
			        }
			    }
		} catch (ParserConfigurationException | SAXException | IOException e)
		{
			e.printStackTrace();
		}   
		 return listeRegions;
	}
	

	public static ArrayList<Updatable> getEventsHistoriques()
	{
		ArrayList<Updatable> listeEvents = new ArrayList<Updatable>();
		try
		{
			File fXmlFile = new File("res/xml/eventsHistoriques.xml");
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder;
			
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();	  
			
			 NodeList events = doc.getElementsByTagName("eventHistorique");
			 for (int temp = 0; temp < events.getLength(); temp++) 
			 {
				Node nodeEvent = events.item(temp);
				
				if (nodeEvent.getNodeType() == Node.ELEMENT_NODE) 
		        {
					Element elementEvent = (Element) nodeEvent;
		            
		            
					//importer condition
					//éventuellement ?: plusieurs conditions
					Condition condition = new ConditionFantoche();
				    NodeList conditionList = elementEvent.getElementsByTagName("condition");
				    Element elementCondition = ((Element) conditionList.item(0));
				    
				    if(conditionList.getLength() > 0)
				    {
				    	switch (elementCondition.getAttribute("type"))
						{
						case "fantoche":
						{
							break;
						}
						case "comparaison":
						{
							Element elementValeurGauche = (Element) elementCondition.getElementsByTagName("valeurGauche").item(0);
							String comparateur = ((Element) elementCondition.getElementsByTagName("comparateur").item(0)).getTextContent();
							Element elementValeurDroite = (Element) elementCondition.getElementsByTagName("valeurDroite").item(0);

							Valeur valeurGauche = null;
							if(elementValeurGauche.getAttribute("typeValeur").equals("variable"))
							{
								valeurGauche = new ValeurJeu(elementValeurGauche.getTextContent());
							}
							else if(elementValeurGauche.getAttribute("typeValeur").equals("constante"))
							{
								valeurGauche = new ValeurConstante(Float.parseFloat(elementValeurGauche.getTextContent()));
							}
							
							Valeur valeurDroite = null;
							if(elementValeurDroite.getAttribute("typeValeur").equals("variable"))
							{
								valeurDroite = new ValeurJeu(elementValeurDroite.getTextContent());
							}
							else if(elementValeurDroite.getAttribute("typeValeur").equals("constante"))
							{
								valeurDroite = new ValeurConstante(Float.parseFloat(elementValeurDroite.getTextContent()));
							}
							
							condition = new ConditionComparaison(valeurGauche, comparateur, valeurDroite);
							
							break;
						}
						default:
							throw new IllegalArgumentException("Unexpected value: " + ((Element) conditionList.item(0)).getAttribute("type"));
						}
				    }
				    
				    NodeList influencesList = elementEvent.getElementsByTagName("influence");
				    ArrayList<Influence> influences = new ArrayList<>();
				    for(int i = 0; i < influencesList.getLength(); i++)
				    {
				    	Node nodeInfluence = influencesList.item(i);
				    	if(nodeInfluence.getNodeType() == Node.ELEMENT_NODE)
				    	{
				    		Element elementInfluence = (Element) nodeInfluence;
				    		
				    		boolean croissant = false;
				    		if(elementInfluence.getAttribute("type").equalsIgnoreCase("croissant"))
				    		{
				    			croissant = true;
				    		}
				    		String nomValeurInfluence = ((Element) elementInfluence.getElementsByTagName("nomValeur").item(0)).getTextContent();
				    		float borneInf = Float.parseFloat(((Element) elementInfluence.getElementsByTagName("borneInf").item(0)).getTextContent());
				    		float borneSup = Float.parseFloat(((Element) elementInfluence.getElementsByTagName("borneSup").item(0)).getTextContent());
				    		Influence influenceTemp = new Influence(nomValeurInfluence, borneInf, borneSup, croissant);
				    		influences.add(influenceTemp);
				    	}
				    }
				    
				    String dateMinTab[] = ((Element) elementEvent.getElementsByTagName("dateMin").item(0)).getTextContent().split("/");
				    DateDonnee dateMin = new DateDonnee(Integer.parseInt(dateMinTab[2]), Integer.parseInt(dateMinTab[1]), Integer.parseInt(dateMinTab[0]), 0, 0);
				    String dateMaxTab[] = ((Element) elementEvent.getElementsByTagName("dateMax").item(0)).getTextContent().split("/");
				    DateDonnee dateMax = new DateDonnee(Integer.parseInt(dateMaxTab[2]), Integer.parseInt(dateMaxTab[1]), Integer.parseInt(dateMaxTab[0]), 0, 0);
				    int borneInf = Integer.parseInt(((Element) elementEvent.getElementsByTagName("heure").item(0)).getTextContent());
				    
				    //importer action
				    Element elementAction = ((Element) elementEvent.getElementsByTagName("action").item(0));
				    Action action = transformerEnAction(elementAction);
				   
				    
				    EventHistorique event = new EventHistorique(condition, influences, dateMin, dateMax, borneInf, action);
				    listeEvents.add(event);
		        }
				
				
			 }
			 
		} catch (ParserConfigurationException | SAXException | IOException e)
		{
			e.printStackTrace();
		}   
		return listeEvents;
	}
	
	public static Action transformerEnAction(Element element)
	{
		Action action = null;
		String type = element.getAttribute("type");
	    if(type.equals("augmenterValeur"))
	    {
	    	action = new ActionAugmenterValeur(((Element) element.getElementsByTagName("nomValeur").item(0)).getTextContent(), Float.parseFloat(((Element) element.getElementsByTagName("quantite").item(0)).getTextContent()));
	    }
	    else if(type.equals("dialogue"))
	    {
	    	String titre = ((Element) element.getElementsByTagName("titre").item(0)).getTextContent();
	    	String textePrincipal = ((Element) element.getElementsByTagName("textePrincipal").item(0)).getTextContent();	
	    	String image = ((Element) element.getElementsByTagName("image").item(0)).getTextContent();
	    	String texteChoix1 = ((Element) element.getElementsByTagName("texteChoix1").item(0)).getTextContent();
	    	String texteChoix2 = ((Element) element.getElementsByTagName("texteChoix2").item(0)).getTextContent();
	    	String texteChoix3 = ((Element) element.getElementsByTagName("texteChoix3").item(0)).getTextContent();
	    	String texteChoix4 = ((Element) element.getElementsByTagName("texteChoix4").item(0)).getTextContent();
	    	
	    	NodeList nodeList = element.getElementsByTagName("action");
	    	Action[] listeActions = new Action[4];
	    	
	    	for(int i = 0; i < nodeList.getLength(); i++)
	    	{
	    		Element elementAction = (Element) nodeList.item(i);
	    		Action actionTemp = transformerEnAction(elementAction);
	    		listeActions[i] = actionTemp;
	    	}
	    	
	    	InfosBoiteDeDialogue infos = new InfosBoiteDeDialogue(texteChoix1, listeActions[0], texteChoix2, listeActions[1], texteChoix3, listeActions[2], texteChoix4, listeActions[3], titre, image, textePrincipal);
	    	action = new ActionDialogue(infos);
	    }
	    else if(type.equals("fantoche"))
	    {
	    	action = new ActionFantoche();
	    }
		
		return action;
	}
	

}
