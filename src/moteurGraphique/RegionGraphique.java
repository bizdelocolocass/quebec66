package moteurGraphique;

public class RegionGraphique
{
	private String nom;
	private float r;
	private float g;
	private float b;
	
	public RegionGraphique(String nom, float r, float g, float b)
	{
		this.nom = nom;
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public String getNom()
	{
		return nom;
	}

	public float getR()
	{
		return r;
	}

	public float getG()
	{
		return g;
	}

	public float getB()
	{
		return b;
	}
}
