package moteurGraphique;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import moteurGraphique.models.RawModel;

public class OBJLoader
{
	
	public static final float minX = -3f;
	public static final float maxX = 1f;
	public static final float minY = -1.5f;
	public static final float maxY = 1.5f;
	
	/**public static final float minX = minMaxRegions()[0];
	public static final float maxX = minMaxRegions()[1];
	public static final float minY = minMaxRegions()[2];
	public static final float maxY = minMaxRegions()[3];**/
	
	public static RawModel loadOBJModel(String filename, Loader loader)
	{
		/**float[] valeursTemp = minMaxRegions();
		float minX = valeursTemp[0];
		float maxX = valeursTemp[1];
		float minY = valeursTemp[2];
		float maxY = valeursTemp[3];**/
		
		FileReader fr = null;
		try
		{
			fr = new FileReader(new File("res/regions/" + filename + ".obj"));
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader reader = new BufferedReader(fr);
			
		String line;
		List<Vec3> vertices = new ArrayList<Vec3>();
		List<Integer> indices = new ArrayList<Integer>();
		float[] verticesArray = null;
		int[] indicesArray = null;
		try
		{
			while(true)
			{
				line = reader.readLine();
				String [] currentLine = line.split(" ");
				if(line.startsWith("v "))
				{
					Vec3 vertex = new Vec3(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
					vertices.add(vertex);
				}
				else if(line.startsWith("f "))
				{
					break;
				}
			}
			
			while(line != null)
			{
				if(!line.startsWith("f "))
				{
					line = reader.readLine();
					continue;
				}
				
				String[] currentLine = line.split(" ");
				String[] vertex1 = currentLine[1].split("/");
				String[] vertex2 = currentLine[2].split("/");
				String[] vertex3 = currentLine[3].split("/");
				
				processVertex(vertex1, indices);
				processVertex(vertex2, indices);
				processVertex(vertex3, indices);
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		verticesArray = new float[vertices.size()*3];
		indicesArray = new int[indices.size()];
		
		float largeurTemp = maxX-minX;
		float hauteurTemp = maxY-minY;
		
		int vertexPointer = 0;
		for(Vec3 vertex:vertices)
		{
			
			verticesArray[vertexPointer++] = (vertex.x - minX)/largeurTemp*2-1;
			verticesArray[vertexPointer++] = -((vertex.z - minY)/hauteurTemp*2)+1;
			verticesArray[vertexPointer++] = vertex.y;
			//tester avec autres coords pour voir
		}
		
		for(int i=0;i<indices.size();i++)
		{
			indicesArray[i] = indices.get(i);
		}
		return loader.loadToVAO(verticesArray, indicesArray);
	}
	
	private static void processVertex(String[] vertexData, List<Integer> indices)
	{
		int currentVertexPointer = Integer.parseInt(vertexData[0]) - 1;
		indices.add(currentVertexPointer);
		
	}
	
	/**
	 * 
	 * @return array de float avec, dans l'ordre, minX, maxX, minY, maxY
	 */
	public static float[] minMaxRegions()
	{
		float minX = 0;
		float maxX = 0;
		float minY = 0;
		float maxY = 0;
		
		FileReader fr = null;
		try
		{
			fr = new FileReader(new File("res/regions/total.obj"));
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader reader = new BufferedReader(fr);
			
		String line;
		List<Vec3> vertices = new ArrayList<Vec3>();
		try
		{
			while(true)
			{
				line = reader.readLine();
				String [] currentLine = line.split(" ");
				if(line.startsWith("v "))
				{
					Vec3 vertex = new Vec3(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
					vertices.add(vertex);
					
					if(vertex.x < minX)
					{
						minX = vertex.x;
					}
					else if(vertex.x > maxX)
					{
						maxX = vertex.x;
					}
					
					if(vertex.z < minY)
					{
						minY = vertex.z;
					}
					else if (vertex.z > maxY)
					{
						maxY = vertex.z;
					}
				}
				else if(line.startsWith("cul"))
				{
					break;
				}
			}
			reader.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return new float[] {minX,maxX,minY,maxY};
	}
}
