package moteurGraphique;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.stb.STBImage;
import org.lwjgl.system.MemoryStack;

import moteurGraphique.models.RawModel;

public class Loader
{
	private List<Integer> vaos = new ArrayList<Integer>();
	private List<Integer> vbos = new ArrayList<Integer>();
	private List<Integer> textures = new ArrayList<Integer>();

	public RawModel loadToVAO(float[] positions, int[] indices)
	{
		int vaoID = createVAO();
		bindIndicesBuffer(indices);
		storeDataInAttributeList(0, 3, positions);
		unbindVAO();
		return new RawModel(vaoID, indices.length);
	}
	
	public RawModel loadToVAO(float[] positions, int[] indices, float[] textureCoords)
	{
		int vaoID = createVAO();
		bindIndicesBuffer(indices);
		storeDataInAttributeList(0, 3, positions);
		storeDataInAttributeList(1, 2, textureCoords);
		unbindVAO();
		return new RawModel(vaoID, indices.length);
	}
	
	public int loadToVAO(float[] positions, float[] textureCoords)
	{
		int vaoID = createVAO();
		storeDataInAttributeList(0, 2, positions);
		storeDataInAttributeList(1, 2, textureCoords);
		unbindVAO();
		return vaoID;
	}
	
	
	public Loader()
	{
		// TODO Auto-generated constructor stub
	}
	
	 public int loadTexture(String path) {
		  
		  int textureID;
		  int width, height;
		  ByteBuffer image;
		        
		        try (MemoryStack stack = MemoryStack.stackPush()) {
		            IntBuffer w = stack.mallocInt(1);
		            IntBuffer h = stack.mallocInt(1);
		            IntBuffer comp = stack.mallocInt(1);

		            image = STBImage.stbi_load("res/"+path+".png", w, h, comp, 4);
		            if (image == null) {
		             System.out.println("Failed to load texture file: "+path+"\n" +
		            		 STBImage.stbi_failure_reason()
		               );
		            }
		            width = w.get();
		            height = h.get();
		        }
		        
		        textureID = GL30.glGenTextures();
		        GL30.glBindTexture(GL30.GL_TEXTURE_2D, textureID);
		        textures.add(textureID);
		        GL30.glTexParameteri(GL30.GL_TEXTURE_2D, GL30.GL_TEXTURE_MIN_FILTER, GL30.GL_NEAREST); //sets MINIFICATION filtering to nearest
		        GL30.glTexParameteri(GL30.GL_TEXTURE_2D, GL30.GL_TEXTURE_MAG_FILTER, GL30.GL_NEAREST); //sets MAGNIFICATION filtering to nearest
		        GL30.glTexImage2D(GL30.GL_TEXTURE_2D, 0, GL30.GL_RGBA, width, height, 0, GL30.GL_RGBA, GL30.GL_UNSIGNED_BYTE, image);
		    
		  return textureID;
		 }
	
	public void nettoyage()
	{
		for(int vao:vaos)
		{
			GL30.glDeleteVertexArrays(vao);
		}
		for(int vbo:vbos)
		{
			GL30.glDeleteBuffers(vbo);
		}
		for(int texture:textures)
		{
			GL30.glDeleteTextures(texture);
		}
	}

	private int createVAO()
	{
		int vaoID = GL30.glGenVertexArrays();
		vaos.add(vaoID);
		GL30.glBindVertexArray(vaoID);
		return vaoID;
	}
	
	private void storeDataInAttributeList(int attributeNumber, int coordSize, float[] data)
	{
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		FloatBuffer buffer = storeDataInFloatBuffer(data);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attributeNumber, coordSize, GL11.GL_FLOAT, false, 0, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	} 
	
	private void unbindVAO()
	{
		GL30.glBindVertexArray(0);
	}
	
	private void bindIndicesBuffer(int[] indices)
	{
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = storeDataInIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}
	
	private IntBuffer storeDataInIntBuffer(int[] data)
	{
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	private FloatBuffer storeDataInFloatBuffer(float[] data)
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public void detruireDerniereJaugeVAO()
	{
		GL30.glDeleteVertexArrays(vaos.get(vaos.size()-1));
		vaos.remove(vaos.size()-1);
		
		GL30.glDeleteBuffers(vbos.get(vbos.size()-1));
		GL30.glDeleteBuffers(vbos.get(vbos.size()-2));
		GL30.glDeleteBuffers(vbos.get(vbos.size()-3));
		vbos.remove(vbos.size()-1);
		vbos.remove(vbos.size()-1);
		vbos.remove(vbos.size()-1);
	}
	
	public void detruireDernierTexteVAO()
	{
		GL30.glDeleteVertexArrays(vaos.get(vaos.size()-1));
		vaos.remove(vaos.size()-1);
		
		GL30.glDeleteBuffers(vbos.get(vbos.size()-1));
		GL30.glDeleteBuffers(vbos.get(vbos.size()-2));
		vbos.remove(vbos.size()-1);
		vbos.remove(vbos.size()-1);
	}
}
