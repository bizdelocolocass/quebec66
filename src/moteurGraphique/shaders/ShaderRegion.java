package moteurGraphique.shaders;

public class ShaderRegion extends ShaderProgram{
	
	private static final String VERTEX_FILE = "src/moteurGraphique/shaders/vertexShaderRegion.txt";
	private static final String FRAGMENT_FILE = "src/moteurGraphique/shaders/fragmentShaderRegion.txt";

	private int locationR;
	private int locationG;
	private int locationB;
	private int locationZoom;
	private int locationDX;
	private int locationDY;
	
	public ShaderRegion() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}
	
	@Override
	protected void getAllUniformLocations()
	{
		locationR = super.getUniformLocation("r");
		locationG = super.getUniformLocation("g");
		locationB = super.getUniformLocation("b");
		locationZoom = super.getUniformLocation("zoom");
		locationDX = super.getUniformLocation("dx");
		locationDY = super.getUniformLocation("dy");
	}

	public void loadR(float r)
	{
		super.loadFloat(locationR, r);
	}
	
	public void loadG(float g)
	{
		super.loadFloat(locationG, g);
	}
	
	public void loadB(float b)
	{
		super.loadFloat(locationB, b);
	}
	
	public void loadZoom(float zoom)
	{
		super.loadFloat(locationZoom, zoom);
	}
	
	//suck it
	public void loadDX(float dx)
	{
		super.loadFloat(locationDX, dx);
	}
	
	public void loadDY(float dy)
	{
		super.loadFloat(locationDY, dy);
	}
}