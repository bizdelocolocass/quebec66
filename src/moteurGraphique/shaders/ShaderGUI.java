package moteurGraphique.shaders;

public class ShaderGUI extends ShaderProgram
{

	private static final String VERTEX_FILE = "src/moteurGraphique/shaders/vertexShaderGUI.txt";
	private static final String FRAGMENT_FILE = "src/moteurGraphique/shaders/fragmentShaderGUI.txt";
	
	public ShaderGUI()
	{
		super(VERTEX_FILE, FRAGMENT_FILE);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void getAllUniformLocations()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void bindAttributes()
	{
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
	}

	
	
}
