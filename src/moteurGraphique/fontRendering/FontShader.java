package moteurGraphique.fontRendering;

import moteurGraphique.Vec3;
import moteurGraphique.shaders.ShaderProgram;

public class FontShader extends ShaderProgram{

	private static final String VERTEX_FILE = "src/moteurGraphique/fontRendering/fontVertex.txt";
	private static final String FRAGMENT_FILE = "src/moteurGraphique/fontRendering/fontFragment.txt";
	
	private int locationR;
	private int locationG;
	private int locationB;
	private int locationDX;
	private int locationDY;
	
	public FontShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void getAllUniformLocations() 
	{
		locationR = super.getUniformLocation("r");
		locationG = super.getUniformLocation("g");
		locationB = super.getUniformLocation("b");
		locationDX = super.getUniformLocation("dx");
		locationDY = super.getUniformLocation("dy");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
	}

	protected void loadColour(float r, float g, float b)
	{
		super.loadFloat(locationR, r);
		super.loadFloat(locationG, g);
		super.loadFloat(locationB, b);
	}
	
	protected void loadTranslation(float x, float y)
	{
		super.loadFloat(locationDX, x);
		super.loadFloat(locationDY, y);
	}

}
