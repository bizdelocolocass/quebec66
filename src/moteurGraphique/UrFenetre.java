package moteurGraphique;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import donnees.actions.ActionAugmenterValeur;
import jeu.Jeu;
import moteurGraphique.shaders.ShaderRegion;

import java.awt.image.renderable.RenderableImageOp;
import java.nio.*;
import java.time.Duration;
import java.time.Instant;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public class UrFenetre
{
	private long window;
	private long monitor;
	private int largeur;
	private int hauteur;
	private Jeu jeu;
	private Instant dernierTour = Instant.now();

	public UrFenetre(int largeur, int hauteur)
	{
		this.largeur = largeur;
		this.hauteur = hauteur;
	}

	public void run() 
	{
		init();
		loop();

		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}

	private void init() 
	{
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // the window will be resizable

		// Create the window
		window = glfwCreateWindow(largeur, hauteur, "Quebec 66", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		//Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
			
		});
		
		glfwSetMouseButtonCallback(window, new GLFWMouseButtonCallback()
		{
			
			@Override
			public void invoke(long window, int button, int action, int mods)
			{
				if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
				{
					DoubleBuffer b1 = BufferUtils.createDoubleBuffer(1);
					DoubleBuffer b2 = BufferUtils.createDoubleBuffer(1);
					glfwGetCursorPos(window, b1, b2);
					float clicX = (float) b1.get()/largeur*400;
					float clicY = (float) (300 - b2.get()/hauteur*300);		
					jeu.clic(clicX, clicY);
				}

			}
		});

		glfwSetScrollCallback(window, new GLFWScrollCallback() 
		{
			@Override
			public void invoke(long window, double xoffset, double yoffset) 
			{
				jeu.zoom((float) yoffset);
			}
		});
		
		glfwSetKeyCallback(window, new GLFWKeyCallback() 
		{
		    @Override
		    public void invoke (long window, int key, int scancode, int action, int mods) 
		    {
		    	if (action == GLFW_PRESS || action == GLFW_REPEAT) 
		    	{
		    		if(key == GLFW_KEY_UP)
		    		{
		    			jeu.enHaut();
		    		}
		    		else if(key == GLFW_KEY_DOWN)
		    		{
		    			jeu.enBas();
		    		}
		    		else if(key == GLFW_KEY_RIGHT)
		    		{
		    			jeu.aDroite();
		    		}
		    		else if(key == GLFW_KEY_LEFT)
		    		{
		    			jeu.aGauche();
		    		}
		    		else if(key == GLFW_KEY_SPACE)
		    		{
		    			jeu.pauseOuStart();
		    		}
		    	}
		    }
		});

		// Get the thread stack and push a new frame
		try ( MemoryStack stack = stackPush() ) {
			IntBuffer pWidth = stack.mallocInt(1); // int*
			IntBuffer pHeight = stack.mallocInt(1); // int*

			// Get the window size passed to glfwCreateWindow
			glfwGetWindowSize(window, pWidth, pHeight);

			// Get the resolution of the primary monitor
			GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// Center the window
			glfwSetWindowPos(
				window,
				(vidmode.width() - pWidth.get(0)) / 2,
				(vidmode.height() - pHeight.get(0)) / 2
			);
		} // the stack frame is popped automatically

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		
		// Make the window visible
		glfwShowWindow(window);
	}

	private void loop() 
	{
		
		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the GLCapabilities instance and makes the OpenGL
		// bindings available for use.
		GL.createCapabilities();
		//glEnable(GL_TEXTURE_2D);
		
		//test
		this.jeu = new Jeu();
		System.out.println(jeu.getValeur("enjeu_monarchie_importance")); //test
		new ActionAugmenterValeur("enjeu_monarchie_importance", 8.0f).executer(jeu);
		System.out.println(jeu.getValeur("enjeu_monarchie_importance")); //test
		System.out.println(jeu.getTexte("enjeu_monarchie_nom"));//test
			
		//Loader loader = new Loader();
		//Renderer renderer = new Renderer();
		//RegionShader shader = new RegionShader(null, null);
		 
		/**
		float[] vertices = 
		{
			-0.5f, 0.5f, 0f,
			-0.5f, -0.5f, 0f,
			0.5f, -0.5f, 0,
			0.5f, 0.5f, 0f
		};
		int[] indices = {0,1,3,3,1,2};
		float[] textureCoords = {0,0,  0,1,  1,1,  1,0};
		
		//RawModel model = loader.loadToVAO(vertices, textureCoords, indices);
		//ModelTexture texture = new ModelTexture(loader.loadTexture("virginie"));
		//TexturedModel texturedModel = new TexturedModel(model,  texture);
		// Run the rendering loop until the user has attempted to close
		// the window or has pressed the ESCAPE key.
		
		Loader loader = new Loader();
		Renderer renderer = new Renderer();
		RawModel model = OBJLoader.loadOBJModel("capitale", loader);
		RawModel model2 = OBJLoader.loadOBJModel("gaspesie", loader);
		ShaderRegion shader = new ShaderRegion ();**/
		
		
		
		while (!glfwWindowShouldClose(window)) 
		{
			
			glfwSwapBuffers(window); // swap the color buffers
			
			DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
			DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
			glfwGetCursorPos(window, xBuffer, yBuffer);
			float x = (float) xBuffer.get(0);
			float y = (float) yBuffer.get(0);
			
			x = x/largeur*400;
			y = 300 - y/hauteur*300;
			
			
			Instant after = Instant.now();
			long delta = Duration.between(dernierTour, after).toMillis();
			dernierTour = after;
			
			jeu.update(delta,x,y);
			// Poll for window events. The key callback above will only be
			// invoked during this call.
			glfwPollEvents();
		}
		
		jeu.finJeu();
	}

	public static void main(String[] args) {
		new UrFenetre(1250, 1000).run();
	}
}
