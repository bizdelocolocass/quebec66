package moteurGraphique;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import carte.Carte;
import carte.Point;
import gui.Bouton;
import gui.Jauge;
import gui.Texte;
import moteurGraphique.fontMeshCreator.FontType;
import moteurGraphique.fontMeshCreator.GUIText;
import moteurGraphique.fontRendering.FontRenderer;
import moteurGraphique.fontRendering.TextMaster;
import moteurGraphique.models.RawModel;
import moteurGraphique.models.TexturedModel;
import moteurGraphique.shaders.ShaderGUI;
import moteurGraphique.shaders.ShaderRegion;
import moteurGraphique.textures.ModelTexture;

public class Dessinatrice
{
	private float[] textureCoords = {0f,0f,
			0f,1f,
			1f,1f,
			1f,0f};

	private Loader loader = new Loader();
	private Renderer renderer = new Renderer();
	private FontRenderer fontRenderer = new FontRenderer();
	private ShaderRegion shaderRegion = new ShaderRegion();
	private ShaderGUI shaderGUI = new ShaderGUI();
	
	private HashMap<String, RawModel> mapRegions = new HashMap<String, RawModel>();
	private HashMap<String, TexturedModel> mapGUI = new HashMap<String, TexturedModel>();
	private HashMap<String, Texte> mapTexte = new HashMap<String, Texte>();
	private HashMap<String, Bouton> mapBouton = new HashMap<String, Bouton>();
	private HashMap<String, Jauge> mapJauge = new HashMap<String, Jauge>();
	
	private ArrayList<RegionGraphique> regionsADessiner = new ArrayList<RegionGraphique>();
	private ArrayList<String> guiADessiner = new ArrayList<String>();
	
	private ModelTexture texturefenetre = new ModelTexture(loader.loadTexture("fenetre"));
	private ModelTexture textureBasJauge = new ModelTexture(loader.loadTexture("basJauge"));
	private ModelTexture textureHautJauge = new ModelTexture(loader.loadTexture("hautJauge"));
	private ModelTexture textureBouton = new ModelTexture(loader.loadTexture("virginie"));
	//polices
	FontType calibri = new FontType(loader.loadTexture("polices/sans"), new File("res/polices/sans.fnt"));
	
	
	public Dessinatrice()
	{
		TextMaster.init(loader);
	}

	public void dessinerTout(Carte carte)
	{	
		renderer.prepare();
		shaderRegion.start();
		shaderRegion.loadZoom(carte.getZoom());
		shaderRegion.loadDX(carte.getDx());
		shaderRegion.loadDY(carte.getDy());
		for(RegionGraphique region: regionsADessiner)
		{
			shaderRegion.loadR(region.getR());
			shaderRegion.loadG(region.getG());
			shaderRegion.loadB(region.getB());
			renderer.render(mapRegions.get(region.getNom()));
		}
		shaderRegion.stop();
		shaderGUI.start();
		
		for(String objet: guiADessiner)
		{
			if(objet.startsWith("texte_"))
			{
				shaderGUI.stop();
				
				fontRenderer.prepare();
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, calibri.getTextureAtlas());
				Texte texteTemp =  mapTexte.get(objet);
				GUIText texteAAfficher = new GUIText(texteTemp.getTexte(), texteTemp.getGrosseurTexte(), calibri, new Point(texteTemp.getX()/200,texteTemp.getY()/150-2), texteTemp.getLargeur()/400, texteTemp.isCentre());
				texteAAfficher.setColour(texteTemp.getR(), texteTemp.getG(), texteTemp.getB());
				
				fontRenderer.renderText(texteAAfficher);
				fontRenderer.endRendering();
				
				shaderGUI.start();
				loader.detruireDernierTexteVAO();
			}
			else if(objet.startsWith("bouton_"))
			{		
				Bouton boutonTemp =  mapBouton.get(objet);
				
				//éventuellement utiliser ses infos genre la texture ou l'état
				renderer.render(mapGUI.get(objet));
			}
			else if(objet.startsWith("jauge_"))
			{		
				Jauge jaugeTemp =  mapJauge.get(objet);
				
				float xTemp = jaugeTemp.getX()/200-1;
				float yTemp = jaugeTemp.getY()/150-1;
				float hauteurTemp = jaugeTemp.getHauteur()/150;
				float largeurTemp = jaugeTemp.getLargeur()/200;
				float milieu = hauteurTemp/100*jaugeTemp.getPourcent();
				TexturedModel modelBas = new TexturedModel(loader.loadToVAO(new float[] {xTemp, yTemp+milieu, 0f, xTemp, yTemp, 0f, xTemp+largeurTemp, yTemp, 0f, xTemp+largeurTemp, yTemp+milieu, 0f}, new int[] {0,1,3,3,1,2}, textureCoords), textureBasJauge);
				TexturedModel modelHaut = new TexturedModel(loader.loadToVAO(new float[] {xTemp, yTemp+hauteurTemp, 0f, xTemp, yTemp+milieu, 0f, xTemp+largeurTemp, yTemp+milieu, 0f, xTemp+largeurTemp, yTemp+hauteurTemp, 0f}, new int[] {0,1,3,3,1,2}, textureCoords), textureHautJauge);

				//éventuellement utiliser ses infos genre la texture ou l'état
				renderer.render(modelBas);
				renderer.render(modelHaut);
				loader.detruireDerniereJaugeVAO();
				loader.detruireDerniereJaugeVAO();
			}
			else
			{
				renderer.render(mapGUI.get(objet));
			}
		}
		shaderGUI.stop();
		
		regionsADessiner.clear();
		guiADessiner.clear();
		//TextMaster.render();
	}
	
	public void dessinerRegion(String nomRegion, float r, float g, float b)
	{
		regionsADessiner.add(new RegionGraphique(nomRegion,r,g,b));
	}
	
	public void dessinerGUI(String nomGUI)
	{
		guiADessiner.add(nomGUI);
	}
	
	public void ajouterRegion(String nom)
	{
		mapRegions.put(nom, OBJLoader.loadOBJModel(nom, loader));
	}
	
	public void ajouterObjetGUI(String nom, float x, float y, float largeur, float hauteur)
	{
		float xTemp = x/200-1;
		float yTemp = y/150-1;
		float hauteurTemp = hauteur/150;
		float largeurTemp = largeur/200;
		mapGUI.put(nom, new TexturedModel(loader.loadToVAO(new float[] {xTemp, yTemp+hauteurTemp, 0f, xTemp, yTemp, 0f, xTemp+largeurTemp, yTemp, 0f, xTemp+largeurTemp, yTemp+hauteurTemp, 0f}, new int[] {0,1,3,3,1,2}, textureCoords), texturefenetre));
	}
	
	public void ajouterObjetGUI(String nom, float x, float y, float largeur, float hauteur, String texture)
	{
		float xTemp = x/200-1;
		float yTemp = y/150-1;
		float hauteurTemp = hauteur/150;
		float largeurTemp = largeur/200;
		mapGUI.put(nom, new TexturedModel(loader.loadToVAO(new float[] {xTemp, yTemp+hauteurTemp, 0f, xTemp, yTemp, 0f, xTemp+largeurTemp, yTemp, 0f, xTemp+largeurTemp, yTemp+hauteurTemp, 0f}, new int[] {0,1,3,3,1,2}, textureCoords), new ModelTexture(loader.loadTexture(texture))));
	}
	
	public void ajouterTexte(Texte texte)
	{
		/**
		float xTemp = texte.getX()/200-1;
		float yTemp = texte.getY()/150-1;
		float hauteurTemp = texte.getHauteur()/150;
		float largeurTemp = texte.getLargeur()/200;
		mapGUI.put(texte.getId(), new TexturedModel(loader.loadToVAO(new float[] {xTemp, yTemp+hauteurTemp, 0f, xTemp, yTemp, 0f, xTemp+largeurTemp, yTemp, 0f, xTemp+largeurTemp, yTemp+hauteurTemp, 0f}, new int[] {0,1,3,3,1,2}, textureCoords), new ModelTexture(loader.loadTexture("virginie"))));
		**/
		mapTexte.put(texte.getId(), texte);
	}
	
	public void ajouterBouton(Bouton bouton)
	{
		
		float xTemp = bouton.getX()/200-1;
		float yTemp = bouton.getY()/150-1;
		float hauteurTemp = bouton.getHauteur()/150;
		float largeurTemp = bouton.getLargeur()/200;
		mapGUI.put(bouton.getId(), new TexturedModel(loader.loadToVAO(new float[] {xTemp, yTemp+hauteurTemp, 0f, xTemp, yTemp, 0f, xTemp+largeurTemp, yTemp, 0f, xTemp+largeurTemp, yTemp+hauteurTemp, 0f}, new int[] {0,1,3,3,1,2}, textureCoords), textureBouton));
		mapBouton.put(bouton.getId(), bouton);
	}
	
	public void ajouterJauge(Jauge jauge)
	{
		mapJauge.put(jauge.getId(), jauge);
	}
	
	public void nettoyage()
	{
		TextMaster.cleanUp();
		fontRenderer.cleanUp();
		shaderRegion.cleanUp();
		shaderGUI.cleanUp();
		loader.nettoyage();
	}
}
